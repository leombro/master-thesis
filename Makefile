CXX ?= g++
FASTFOLDER ?= /usr/local/fastflow
BSPFOLDER := include/
CXXFLAGS := -std=c++17 -O3
BSPFLAGS := -pthread -I$(FASTFOLDER) -I$(BSPFOLDER)

OBJDIR := bin
OTHERDIR := src
OTHERNAMES := $(basename $(notdir $(shell echo $(OTHERDIR)/*.cpp)))
OTHEROBJECTS := $(addprefix $(OBJDIR)/other/,$(OTHERNAMES))
EDUPACKDIR := src/bsp/bspedupack
EDUPACKNAMES := $(basename $(notdir $(shell echo $(EDUPACKDIR)/*.cpp)))
EDUPACKOBJECTS := $(addprefix $(OBJDIR)/bspedupack/,$(EDUPACKNAMES))
BSPDIR := src/bsp
BSPNAMES := $(basename $(notdir $(shell echo $(BSPDIR)/*.cpp)))
BSPOBJECTS := $(addprefix $(OBJDIR)/bsp/,$(BSPNAMES))
SEQDIR := src/seq
SEQNAMES = $(basename $(notdir $(shell echo $(SEQDIR)/*.cpp)))
SEQOBJECTS := $(addprefix $(OBJDIR)/seq/,$(SEQNAMES))

.PHONY: all bspall bspedupack bspother seq test testinprod \
		testsort testfft testlu cleanall cleantests cleanbsp cleanseq debug

all: bspall seq

bspall: bspedupack bspother

bspedupack: $(EDUPACKOBJECTS)

bspother: $(OTHEROBJECTS) $(BSPOBJECTS)

seq: $(SEQOBJECTS)

test: testinprod testsort testfft testlu

testfft: $(OBJDIR)/bspedupack/BSPfft $(OBJDIR)/seq/sequential_fft
	@echo "Starting test: FFT"
	@tests/test_fft.sh

testinprod: $(OBJDIR)/bspedupack/BSPinprod $(OBJDIR)/seq/sequential_inprod
	@echo "Starting test: inner product"
	@tests/test_inprod.sh

testlu: $(OBJDIR)/bspedupack/BSPlu $(OBJDIR)/seq/sequential_lu
	@echo "Starting test: LU decomposition"
	@tests/test_lu.sh

testsort: $(OBJDIR)/bsp/BSPpsrs $(OBJDIR)/seq/sequential_sorting
	@echo "Starting test: sorting by regular sampling"
	@tests/test_sort.sh

cleanall: cleantests cleanbsp cleanseq
	$(if $(shell ! test -d "bin" && echo " "),,$(if $(shell ls bin),,$(shell rmdir bin)))

cleantests:
	@rm -rf tests/logs

cleanbsp:
	@rm -rf bin/other
	@rm -rf bin/bsp
	@rm -rf bin/bspedupack
	$(if $(shell ! test -d "bin" && echo " "),,$(if $(shell ls bin),,$(shell rmdir bin)))

cleanseq:
	@rm -rf bin/seq
	$(if $(shell ! test -d "bin" && echo " "),,$(if $(shell ls bin),,$(shell rmdir bin)))

$(OBJDIR)/other/%: $(OTHERDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@mkdir -p $(OBJDIR)/other
	$(CXX) $(CXXFLAGS) $(BSPFLAGS) $< -o $@

$(OBJDIR)/bspedupack/%: $(EDUPACKDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@mkdir -p $(OBJDIR)/bspedupack
	$(CXX) $(CXXFLAGS) $(BSPFLAGS) $< -o $@

$(OBJDIR)/bsp/%: $(BSPDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@mkdir -p $(OBJDIR)/bsp
	$(CXX) $(CXXFLAGS) $(BSPFLAGS) $< -o $@

$(OBJDIR)/seq/%: $(SEQDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@mkdir -p $(OBJDIR)/seq
	$(CXX) $(CXXFLAGS) $< -o $@
