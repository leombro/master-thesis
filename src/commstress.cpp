#include <bsp_program.hpp>
#include <random>
#include <iostream>

/**
 * benchmarks the communication aspects of the bsp pattern
 */
struct communication_stress : public bsp_node {
    int NITERS = 10000;

    void parallel_function() override {
        using clock = std::chrono::high_resolution_clock;
        using duration = std::chrono::nanoseconds;

        std::random_device rd;
        std::mt19937 mersenne(rd());
        std::uniform_int_distribution uid(0, NITERS - 1);

        long long int rng_duration = 0;
        if (bsp_pid() == 0) {
            // benchmarking the time spent on an RNG call
            int x = 0;
            auto rng_start = clock::now();
            for (int i{0}; i < NITERS; ++i) {
                if (i % 2) x += uid(mersenne);
                else x -= uid(mersenne);
            }
            auto rng_stop = clock::now();
            auto dur = std::chrono::duration_cast<duration>(
                    rng_stop - rng_start).count();
            rng_duration = dur / NITERS;
            // Use the value
            std::cout << "RNG benchmark: a single RNG call takes "
                      << rng_duration << " ns (value is " << x
                      << "), whole op takes "
                      << dur << "ns" << std::endl;
        }

        // Test #1: insertion of random values inside random workers' variables

        bsp_variable<int> var = get_variable<int>(0);

        auto start = clock::now();
        for (size_t i{0}; i < NITERS; ++i) {
            size_t next = uid(mersenne) % bsp_nprocs();
            var.bsp_put(uid(mersenne), next);
            bsp_sync();
        }

        if (bsp_pid() == 0) {
            auto time = std::chrono::duration_cast<duration>(
                    clock::now() - start).count() - (2 * NITERS * rng_duration);
            std::cout << "--- PHASE 1 - VARIABLES ---" << std::endl;
            std::cout << "Spent " << time << "ns" << std::endl;
            std::cout << "That is, " << time / NITERS << "ns per operation"
                      << std::endl;
        }

        // Test #2: replacement of arrays inside random worker's memory

        bsp_array<int> arr1 = get_empty_array<int>(NITERS);
        std::vector<int> swap(NITERS);

        auto gen = [&]() { return uid(mersenne); };
        std::generate(swap.begin(), swap.end(), gen);

        start = clock::now();
        for (size_t i{0}; i < NITERS; ++i) {
            size_t next = uid(mersenne) % bsp_nprocs();
            arr1.bsp_put(swap, next);
            bsp_sync();
        }

        if (bsp_pid() == 0) {
            auto time = std::chrono::duration_cast<duration>(
                    clock::now() - start).count() - (NITERS * rng_duration);
            std::cout << "--- PHASE 2 - ARR_SWAP ---" << std::endl;
            std::cout << "Spent " << time << "ns" << std::endl;
            std::cout << "That is, " << time / NITERS << "ns per operation"
                      << std::endl;
        }

        // Test #3: replacement of portion of arrays inside random positions in a random worker's memory

        bsp_array<int> arr2 = get_empty_array<int>(NITERS);
        std::vector<int> empl(10);
        std::generate(empl.begin(), empl.end(), gen);

        start = clock::now();
        for (size_t i{0}; i < NITERS; ++i) {
            size_t next = uid(mersenne) % bsp_nprocs();
            size_t pos = uid(mersenne) % (NITERS - 10);
            arr2.bsp_put(empl, next, pos, 10);
            bsp_sync();
        }

        if (bsp_pid() == 0) {
            auto time = std::chrono::duration_cast<duration>(
                    clock::now() - start).count() - (2 * NITERS * rng_duration);
            std::cout << "--- PHASE 3 - ARR_EMPLACE ---" << std::endl;
            std::cout << "Spent " << time << "ns" << std::endl;
            std::cout << "That is, " << time / NITERS << "ns per operation"
                      << std::endl;
        }

        // Test #4: replacement of elements in random positions in arrays inside random worker's memory

        bsp_array<int> arr3 = get_empty_array<int>(NITERS);

        start = clock::now();
        for (size_t i{0}; i < NITERS; ++i) {
            size_t next = uid(mersenne) % bsp_nprocs();
            for (size_t j{0}; j < 50; ++j) {
                size_t pos = uid(mersenne) % NITERS;
                arr3.bsp_put(bsp_pid(), next, pos);
            }
            bsp_sync();
        }

        if (bsp_pid() == 0) {
            auto time = std::chrono::duration_cast<duration>(
                    clock::now() - start).count() - (NITERS * rng_duration) -
                        (50 * NITERS * rng_duration);
            std::cout << "--- PHASE 4 - ARR_PUT ---" << std::endl;
            std::cout << "Spent " << time << "ns" << std::endl;
            std::cout << "That is, " << time / (NITERS * 50)
                      << "ns per operation" << std::endl;
        }
    }
};

int main() {
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (size_t i{0}; i < 4; ++i) {
        nodes.push_back(std::make_unique<communication_stress>());
    }
    bsp_program mbsp(std::move(nodes));
    mbsp.start();
}