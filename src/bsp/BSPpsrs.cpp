#include <random>
#include <chrono>
#include <bsp_program.hpp>

struct BSPpsrs : public bsp_node {

    int n;
    int seed;

    explicit BSPpsrs(int problem_size, int random_seed = -1) :
            n{problem_size},
            seed{random_seed} {
    };

    void parallel_function() override {

        bsp_array<int> to_sort = get_empty_array<int>(1);
        int p = bsp_nprocs();
        int s = bsp_pid();
        auto t = std::chrono::high_resolution_clock::now();

        if (s == 0) {
            if (seed == -1) {
                std::random_device x;
                seed = x();
            }
            std::mt19937 mtw(seed);
            std::vector<int> data(n);
            std::iota(data.begin(), data.end(), 0);
            std::shuffle(data.begin(), data.end(), mtw);

#ifdef DEBUG
            std::ofstream print1{"generated_array.log"};
            for (const auto& el: data) print1 << el << std::endl;
            print1.close();
#endif
            auto t2 = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::high_resolution_clock::now() - t).count();
            std::cout << "Ended vector generation and shuffling (spent " << t2
                      << " ms)" <<
                      "\nstarting parallel part" << std::endl;

            t = std::chrono::high_resolution_clock::now();
            int numels = n / p;
            int count = 0;
            for (int i{0}; i < n; i += numels) {
                auto last = std::min(n, i + numels);
                to_sort.bsp_put(
                        std::vector<int>(data.begin() + i, data.begin() + last),
                        count++);
            }
        }

        bsp_sync();

        bsp_array<std::vector<int>> ps_array = get_empty_array<std::vector<int>>(
                p); // array of vectors!

        auto& vec = to_sort.BSPunsafe_access();

        std::sort(vec.begin(), vec.end());

#ifdef DEBUG
        std::ofstream print1a{"distributed_array-" + std::to_string(s) + ".log"};
        for (const auto& el: vec) print1a << el << std::endl;
        print1a.close();
#endif

        std::vector<int> primary_samples;

        size_t samplesize = vec.size() / p;
        size_t i;

        for (i = 0; i < vec.size(); i += samplesize) {
            primary_samples.emplace_back(vec.at(i));
        }

        if (i != vec.size() - 1) primary_samples.emplace_back(vec.at(i - 1));

#ifdef DEBUG
        std::ofstream print2{"primary_samples-" + std::to_string(s) + ".log"};
        for (const auto& el: primary_samples) print2 << el << std::endl;
        print2.close();
#endif

        for (i = 0; i < p; ++i) {
            ps_array.bsp_put(primary_samples, i, s);
        }

        bsp_sync();

        bsp_array<std::vector<int>> portion = get_empty_array<std::vector<int>>(
                p);

        std::vector<int> ps_all;
        std::vector<int> secondary_samples;
        const auto& psref = ps_array.BSPunsafe_access();

        for (const auto& vecs: psref) {
            ps_all.insert(ps_all.end(), vecs.begin(), vecs.end());
        }

        std::sort(ps_all.begin(), ps_all.end());

        samplesize = ps_all.size() / p;

        for (i = 0; i < ps_all.size(); i += samplesize) {
            secondary_samples.emplace_back(ps_all.at(i));
        }

        if (i == ps_all.size())
            secondary_samples.emplace_back(ps_all.at(i - 1));

#ifdef DEBUG
        std::ofstream print3{"secondary_samples-" + std::to_string(s) + ".log"};
        for (const auto& el: secondary_samples) print3 << el << std::endl;
        print3.close();
#endif

        int upperbound;
        int count = 1;

        do {
            upperbound = secondary_samples.at(count++);
        } while (vec.at(0) > upperbound);

        count--;
        std::vector<int> temp;
        for (i = 0; i < vec.size(); ++i) {
            if (vec.at(i) > upperbound) {
                portion.bsp_put(temp, count - 1, s);
                temp.clear();
                upperbound = secondary_samples.at(++count);
            }
            temp.emplace_back(vec.at(i));
        }

        portion.bsp_put(temp, count - 1, s);
        bsp_sync();

        bsp_array<std::vector<int>> final_arr = get_empty_array<std::vector<int>>(
                p);

        std::vector<int> secondary_block;

        auto& pbls_ref = portion.BSPunsafe_access();

        size_t totalsz = 0;
        for (const auto& pbl: pbls_ref) totalsz += pbl.size();

        secondary_block.reserve(totalsz);
        for (auto& pbl: pbls_ref) {
            secondary_block.insert(
                    secondary_block.end(),
                    std::make_move_iterator(pbl.begin()),
                    std::make_move_iterator(pbl.end()));
        }

        std::sort(secondary_block.begin(), secondary_block.end());

#ifdef DEBUG
        std::ofstream print4{"secondary_block-" + std::to_string(s) + ".log"};
        for (const auto& el: secondary_block) print4 << el << std::endl;
        print4.close();
#endif

        final_arr.bsp_put(secondary_block, 0, s);
        bsp_sync();

        if (s == 0) {
            auto& sbls_ref = final_arr.BSPunsafe_access();

            std::vector<int> final;
            for (auto& sbl: sbls_ref) {
                final.insert(final.end(), std::make_move_iterator(sbl.begin()),
                             std::make_move_iterator(sbl.end()));
            }

#ifdef DEBUG
            std::ofstream print5{"final.log"};
            for (const auto& el: final) print5 << el << std::endl;
            print5.close();
#endif

            bool passed = true;
            for (int j{0}; j < n; j++) {
                if (final[j] != j) {
                    passed = false;
                    break;
                }
            }

            std::cout << "Check " << (passed ? "passed" : "failed")
                      << std::endl;
            auto t1 = std::chrono::high_resolution_clock::now();
            std::cout << "Parallel part took "
                      << std::chrono::duration_cast<std::chrono::milliseconds>(
                              t1 - t).count() << " ms." << std::endl;
        }

        bsp_sync(); // Needed to keep all threads alive!
    }

};

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <N> <P> (seed)" << std::endl;
        std::cerr << " where N is the problem size (power of 2)" << std::endl;
        std::cerr << "       P is the number of threads used (power of 2)"
                  << std::endl;
        std::cerr << "       N must be >= P^3" << std::endl;
        std::cerr
                << "       seed is an optional seed for permutations (leave blank to randomize it)"
                << std::endl;
        return -1;
    }
    int n, p, s;
    n = std::stoi(argv[1]);
    p = std::stoi(argv[2]);
    if (n < p * p * p) {
        std::cerr << "N must be >= P^3" << std::endl;
        return -1;
    }
    s = (argc >= 4 ? std::stoi(argv[3]) : -1);
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (size_t i{0}; i < p; ++i) {
        nodes.push_back(std::make_unique<BSPpsrs>(n, s));
    }

    bsp_program mbsp(std::move(nodes));
    mbsp.start();
    return 0;
}