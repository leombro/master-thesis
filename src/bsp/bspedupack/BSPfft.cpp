#include <cmath>
#include <fstream>
#include <bsp_program.hpp>

struct BSPfft : public bsp_node {

    std::vector<double> global_x;
    int global_n;

    explicit BSPfft(int _n, std::vector<double> glob) : global_n{_n}, global_x{
            std::move(glob)} {
    }

    constexpr static double PI = 3.141592653589793;

    static void ufft(std::vector<double>& x, int offset, int n, bool sign,
                     const std::vector<double>& w) {

        for (int k = 2; k <= n; k *= 2) {
            int nk = n / k;
            for (int r = 0; r < nk; ++r) {
                int rk = 2 * r * k;
                for (int j = 0; j < k; j += 2) {
                    double wr = w[j * nk];
                    double wi;
                    if (sign) {
                        wi = w[j * nk + 1];
                    } else {
                        wi = -w[j * nk + 1];
                    }

                    int j0 = rk + j + offset;
                    int j1 = j0 + 1;
                    int j2 = j0 + k;
                    int j3 = j2 + 1;

                    double taur = wr * x[j2] - wi * x[j3];
                    double taui = wi * x[j2] + wr * x[j3];

                    x[j2] = x[j0] - taur;
                    x[j3] = x[j1] - taui;
                    x[j0] += taur;
                    x[j1] += taui;
                }
            }
        }
    }

    static void ufft_init(int n, std::vector<double>& w) {
        assert(w.size() == n);

        if (n == 1) return;

        w[0] = 1.0;
        w[1] = 0.0;

        if (n == 4) {
            w[2] = 0.0;
            w[3] = -1.0;
        } else if (n >= 8) {
            double theta = -2.0 * PI / static_cast<double>(n);
            for (int j = 1; j <= n / 8; j++) {
                w[2 * j] = std::cos(j * theta);
                w[2 * j + 1] = std::sin(j * theta);
            }
            for (int j = 0; j < n / 8; j++) {
                int n4j = n / 4 - j;
                w[2 * n4j] = -w[2 * j + 1];
                w[2 * n4j + 1] = -w[2 * j];
            }
            for (int j = 1; j < n / 4; j++) {
                int n2j = n / 2 - j;
                w[2 * n2j] = -w[2 * j];
                w[2 * n2j + 1] = w[2 * j + 1];
            }
        }
    }

    static void twiddle(std::vector<double>& x, int length, bool sign,
                        const std::vector<double>& w, int offset) {
        for (int jo = 0; jo < 2 * length; jo += 2) {
            int j = jo;
            int j1 = j + 1;
            double wr = w[offset + j];
            double wi;

            if (sign) {
                wi = w[offset + j1];
            } else {
                wi = -w[offset + j1];
            }

            double xr = x[j];
            double xi = x[j1];
            x[j] = wr * xr - wi * xi;
            x[j1] = wi * xr + wr * xi;
        }
    }

    static void twiddle_init(int n, double alpha, const std::vector<int>& rho,
                             std::vector<double>& w, int offset) {
        double theta = -2.0 * PI * alpha / static_cast<double>(n);
        for (int j = 0; j < n; ++j) {
            double rt = static_cast<double>(rho[j]) * theta;
            w[offset + 2 * j] = std::cos(rt);
            w[offset + 2 * j + 1] = std::sin(rt);
        }
    }

    static void
    permute(std::vector<double>& x, int n, const std::vector<int>& sigma,
            int line) {
        assert(x.size() / 2 == sigma.size());

        for (int j = 0; j < n; ++j) {
            if (j < sigma[j]) {
                int j0 = 2 * j;
                int j1 = j0 + 1;
                int j2 = 2 * sigma[j];
                int j3 = j2 + 1;
                double tmpr = x[j0];
                double tmpi = x[j1];
                x[j0] = x[j2];
                x[j1] = x[j3];
                x[j2] = tmpr;
                x[j3] = tmpi;
            }
        }
    }

    static void bitrev_init(std::vector<int>& rho) {
        int n = rho.size();

        auto binary_len = static_cast<int>(std::ceil(
                std::log(static_cast<double>(n)) / std::log(2.0)));
        std::vector<bool> bits(binary_len);
        std::vector<int> pwrs(binary_len);
        pwrs[0] = 1;
        for (int j = 1; j < binary_len; ++j) {
            pwrs[j] = pwrs[j - 1] * 2;
        }
        int j = 0;
        while (j < n - 1) {
            j++;
            int lastbit = 0;
            while (bits[lastbit]) {
                bits[lastbit] = false;
                lastbit++;
            }
            bits[lastbit] = true;
            int val = 0;
            for (int k = 0; k < binary_len; ++k) {
                if (bits[k]) {
                    val += pwrs[binary_len - k - 1];
                }
            }
            rho[j] = val;
        }
    }

    static int k1_init(int n, int p) {
        assert(p < n);

        int np = n / p;
        int c;
        for (c = 1; c < p; c *= np);
        return n / c;
    }

    static void bspfft_init(int n, int p, int s, std::vector<double>& w0,
                            std::vector<double>& w,
                            std::vector<double>& tw, std::vector<int>& rho_np,
                            std::vector<int>& rho_p) {
        int np = n / p;
        bitrev_init(rho_np);

        if (p > 1) {
            bitrev_init(rho_p);
        }
        int k1 = k1_init(n, p);
        ufft_init(k1, w0);
        ufft_init(np, w);

        int ntw = 0;
        for (int c = k1; c <= p; c *= np) {
            double alpha = static_cast<double>(s % c) / static_cast<double>(c);
            twiddle_init(np, alpha, rho_np, tw, 2 * ntw * np);
            ntw++;
        }
    }

    static void calcError(const std::vector<double>& xlocal,
                          const std::vector<double>& xarr, int n, int p,
                          int s) {
        double error = 0.0;
        int c = 0;
        for (; c <= n / p; c++) {
            double lerror = std::abs(xlocal[c] - xarr[c]);
            error += lerror;
        }
        std::cout << s << ": local error is "
                  << (error / static_cast<double>(n)) << std::endl;
    }

    void bspredistr(bsp_array<double>& x, int n, int p, int s, int c0, int c1,
                    bool rev, const std::vector<int>& rho_p) {
        assert(1 <= c0);
        assert(c0 <= c1);
        assert(c1 <= p);

        auto xarr = x.BSPunsafe_access();
        int np = (int) xarr.size() / 2;
        int ratio = c1 / c0;
        int size = std::max(np / ratio, 1);
        int npackets = np / size;
        std::vector<double> tmp(2 * size);

        assert (p <= n);

        int j0, j2;
        if (rev) {
            j0 = rho_p[s] % c0;
            j2 = rho_p[s] / c0;
        } else {
            j0 = s % c0;
            j2 = s / c0;
        }

        for (int j = 0; j < npackets; j++) {
            int jglob = j2 * c0 * np + j * c0 + j0;
            int destproc = (jglob / (c1 * np)) * c1 + jglob % c1;
            int destindex = (jglob % (c1 * np)) / c1;
            for (int r = 0; r < size; ++r) {
                int tr = 2 * r;
                int tjrr = 2 * (j + r * ratio);
                tmp[tr] = xarr[tjrr];
                tmp[tr + 1] = xarr[tjrr + 1];
            }
            assert(destproc <= p);
            assert(destindex < xarr.size() / 2);
            x.bsp_put(tmp, destproc, 0, 2 * destindex, 2 * size);
        }
        bsp_sync();
    }

    void bspfft(bsp_array<double>& x, int n, int p, int s, bool sign,
                const std::vector<double>& w0, const std::vector<double>& w,
                const std::vector<double>& tw,
                const std::vector<int>& rho_np, const std::vector<int>& rho_p) {
        int np = n / p;
        int k1 = k1_init(n, p);
        permute(x.BSPunsafe_access(), np, rho_np, __LINE__);
        bool rev = true;

        for (int r = 0; r < np / k1; r++) {
            ufft(x.BSPunsafe_access(), 2 * r * k1, k1, sign, w0);
        }

        int c0 = 1;
        int ntw = 0;

        for (int c = k1; c <= p; c *= np) {
            bspredistr(x, n, p, s, c0, c, rev, rho_p);
            rev = false;
            twiddle(x.BSPunsafe_access(), np, sign, tw, 2 * ntw * np);
            ufft(x.BSPunsafe_access(), 0, np, sign, w);
            c0 = c;
            ntw++;
        }

        if (!sign) {
            auto& xarr = x.BSPunsafe_access();
            double ninv = 1.0 / static_cast<double>(n);
            for (int j = 0; j < 2 * np; ++j) {
                xarr[j] *= ninv;
            }
        }
    }

    std::vector<double>& fft(const std::vector<double>& xlocal) {
        int s = bsp_pid();

        int n = global_n / 2;
        int p = bsp_nprocs();
        int k1 = k1_init(n, p);
        std::vector<double> w0(k1);
        std::vector<double> w(n / p);
        std::vector<double> tw(2 * n / p);
        std::vector<int> rho_np(n / p);
        std::vector<int> rho_p(p);

        bsp_array<double> x = get_array(xlocal);

        auto time = std::chrono::high_resolution_clock::now();
        if (p == 1) {
            bspfft_init(n, p, s, w0, w, tw, rho_np, rho_p);
            permute(x.BSPunsafe_access(), n, rho_np, __LINE__);
            ufft(x.BSPunsafe_access(), 0, n, true, w);
            permute(x.BSPunsafe_access(), n, rho_np, __LINE__);
            ufft(x.BSPunsafe_access(), 0, n, false, w);
            auto& xi = x.BSPunsafe_access();
            double ninv = 1.0 / static_cast<double>(n);
            for (int j = 0; j < 2 * n; ++j) {
                xi[j] *= ninv;
            }
            calcError(x.BSPunsafe_access(), global_x, n, p, s);
            auto t = std::chrono::high_resolution_clock::now();
            auto tc = std::chrono::duration_cast<std::chrono::milliseconds>(
                    t - time).count();
            std::cout << "Parallel part took " << tc << " ms." << std::endl;
            return x.BSPunsafe_access();
        }

        bsp_sync();

        bspfft_init(n, p, s, w0, w, tw, rho_np, rho_p);
        std::cout << s << ": calling bspfft with n=" << n << ", p=" << p
                  << ", s=" << s << std::endl;
        bspfft(x, n, p, s, true, w0, w, tw, rho_np, rho_p);
        bsp_sync();

        std::cout << s << ": calling bspfft (inv) with n=" << n << ", p=" << p
                  << ", s=" << s << std::endl;
        bspfft(x, n, p, s, false, w0, w, tw, rho_np, rho_p);

        if (s == 0) {
            auto t = std::chrono::high_resolution_clock::now();
            auto tc = std::chrono::duration_cast<std::chrono::milliseconds>(
                    t - time).count();
            std::cout << "Parallel part took " << tc << " ms." << std::endl;
        }

        calcError(x.BSPunsafe_access(), xlocal, n, p, s);
        return x.BSPunsafe_access();
    }

    void parallel_function() override {

        bsp_array<double> local_x = get_empty_array<double>(1);
        if (bsp_pid() == 0) {
            auto p = bsp_nprocs();
            for (int s = 0; s < p; ++s) {
                std::vector<double> xlocal(global_n / p);
                int c = 0;
                int n1 = global_n / 2;
                for (int i = 0; i < 2 * n1; ++i) {
                    if ((i / 2) % p == s) {
                        xlocal[c++] = global_x[i];
                    }
                }
                local_x.bsp_put(xlocal, s);
            }
        }

        bsp_sync();
        fft(local_x.BSPunsafe_access());

    }
};

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <N> <P>" << std::endl;
        std::cerr << " where N is the problem size (power of 2)" << std::endl;
        std::cerr << "       P is the number of threads used (power of 2)"
                  << std::endl;
        return -1;
    }
    int n, p;
    n = std::stoi(argv[1]);
    p = std::stoi(argv[2]);
    std::vector<double> global_x(n);
    for (int i = 0; i < n; i += 2) {
        global_x[i] = static_cast<double>(i) / 2.0;
    }

    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (size_t i{0}; i < p; ++i) {
        nodes.push_back(std::make_unique<BSPfft>(n, global_x));
    }

    bsp_program mbsp(std::move(nodes));
    mbsp.start();
}