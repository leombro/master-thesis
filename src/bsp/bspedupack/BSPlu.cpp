#include <bsp_program.hpp>
#include <cmath>
#include <limits>
#include <random>

class BSPlu : public bsp_node {

private:

    int param_n, param_M, param_N;
    std::mt19937 rand;
    std::uniform_real_distribution<double> dist{-32.768, 32.768};

    template<typename T>
    using matrix = std::vector<std::vector<T>>;

    using clock = std::chrono::high_resolution_clock;
    using milliseconds = std::chrono::milliseconds;

    inline int nloc(int p, int s, int n) const {
        return (n + p - s - 1) / p;
    }

    void bsplu(int M, int N, int n, int s, int t,
               std::vector<int>& raw_pi, matrix<double>& a) {

        int p = bsp_pid();
        int P = bsp_nprocs();
        int p_i = p / N;
        int p_j = p % N;

        assert(p_i == s);
        assert(p_j == t);

        auto pi = get_array<int>(&raw_pi);

        std::vector<bsp_array<double>> A;
        for (int i{0}; i < a.size(); ++i) {
            A.emplace_back(get_array<double>(&a.at(i)));
        }
        std::vector<double> raw_pivots(M);
        auto pivots = get_array<double>(&raw_pivots);
        std::vector<int> raw_pivot_info(3);
        auto pivot_info = get_array<int>(&raw_pivot_info);
        std::vector<double> raw_pivot_row(n / N);
        auto pivot_row = get_array<double>(&raw_pivot_row);
        std::vector<double> raw_pivot_col(n / M);
        auto pivot_col = get_array<double>(&raw_pivot_col);

        bsp_sync();


        for (int i{0}; i < nloc(P, p, n); ++i) {
            raw_pi.at(i) = p + i * P;
        }

        for (int k{0}; k < n - 1; ++k) {
            int kdN = k / N;
            raw_pivot_info.at(0) = nloc(M, p_i, k);
            int kdM = raw_pivot_info.at(0);
            double temp_m;
            double absmax = 0.0;
            if (p_j == k % N) {
                for (int i{raw_pivot_info.at(0)}; i < a.size(); ++i) {
                    if (std::abs(a.at(i).at(kdN)) > absmax) {
                        raw_pivot_info.at(0) = i;
                        absmax = std::abs(a.at(i).at(kdN));
                    }
                }
                temp_m = (raw_pivot_info.at(0) == a.size()) ?
                         -std::numeric_limits<double>::infinity() :
                         a.at(raw_pivot_info.at(0)).at(kdN);
                for (int i{0}; i < M; ++i) {
                    pivots.bsp_put(temp_m, i * N + p_j, p_i);
                }
            }

            bsp_sync();

            raw_pivot_info.at(1) = p_i;
            raw_pivot_info.at(2) = k % N;
            if (p_j == raw_pivot_info.at(2)) {
                temp_m = raw_pivots.at(0);
                raw_pivot_info.at(1) = 0;
                for (int i{1}; i < M; ++i) {
                    double temp = raw_pivots.at(i);
                    if (temp > temp_m) {
                        temp_m = temp;
                        raw_pivot_info.at(1) = i;
                    }
                }
                auto el = pivot_info.bsp_direct_get(
                        raw_pivot_info.at(1) * N + raw_pivot_info.at(2), 0);
                pivot_info.BSPunsafe_access().at(0) = el;
                for (int i{kdM}; i < a.size(); ++i) {
                    a.at(i).at(kdN) /= temp_m;
                }
                if (p_i == raw_pivot_info.at(1)) {
                    a.at(raw_pivot_info.at(0)).at(kdN) = temp_m;
                }

                for (int j{0}; j < N; j++) {
                    pivots.bsp_put(raw_pivots, p_i * N + j,
                                   raw_pivot_info.at(1), raw_pivot_info.at(1),
                                   1);
                    pivot_info.bsp_put(raw_pivot_info, p_i * N + j);
                }
            }

            bsp_sync();

            temp_m = raw_pivots.at(raw_pivot_info.at(1));

            assert(temp_m != 0.0);

            int temp_m_rho =
                    (M * raw_pivot_info.at(0) + raw_pivot_info.at(1)) % P;
            int temp_l_cyc =
                    (M * raw_pivot_info.at(0) + raw_pivot_info.at(1)) / P;

            if (p == temp_m_rho) {
                pi.bsp_get(k % P, k / P, temp_l_cyc, 1);
            }
            if (p == k % P) {
                pi.bsp_get(temp_m_rho, temp_l_cyc, k / P, 1);
            }
            if (p_i == raw_pivot_info.at(1)) {
                auto arr = A.at(k / M).bsp_direct_get((k % M) * N + p_j);
                if (arr.size() == 2)
                    std::cout << "size 2" << std::endl;
                A.at(raw_pivot_info.at(0))
                        .bsp_put(arr);
            }
            if (p_i == k % M) {
                auto arr = A.at(raw_pivot_info.at(0)).bsp_direct_get(
                        raw_pivot_info.at(1) * N + p_j);
                if (arr.size() == 2)
                    std::cout << "size 2" << std::endl;
                A.at(k / M)
                        .bsp_put(arr);
            }

            if (p_i == raw_pivot_info.at(1)) {
                for (int j{0}; j < a.at(0).size(); ++j) {
                    raw_pivot_row.at(j) = a.at(raw_pivot_info.at(0)).at(j);
                }
                for (int i{0}; i < M; ++i) {
                    if (i == p_i) continue;
                    pivot_row.bsp_put(raw_pivot_row, i * N + p_j);
                }
            }

            if (p_j == raw_pivot_info.at(2)) {
                for (int i{0}; i < a.size(); ++i) {
                    raw_pivot_col.at(i) = a.at(i).at(k / N);
                }
                for (int j{0}; j < N; ++j) {
                    if (j == p_j) continue;
                    pivot_col.bsp_put(raw_pivot_col, p_i * N + j);
                }
            }

            bsp_sync();

            if (p_i == raw_pivot_info.at(1)) {
                auto el = A.at(raw_pivot_info.at(0)).bsp_direct_get(
                        p_i * N + k % N);
                std::copy_n(el.begin() + k / N, 1,
                            raw_pivot_col.begin() + raw_pivot_info.at(0));
                //pivot_col.bsp_put(el, k/N, raw_pivot_info.at(0), 1);
            }

            bsp_sync();

            int istart = k / M;
            int jstart = k / N;
            // a.at(istart) = A.at(istart).BSPunsafe_access();
            if (p_i <= k % M) istart++;
            if (p_j <= k % N) jstart++;
            auto& raw_pivot_row2 = pivot_row.BSPunsafe_access();
            auto& raw_pivot_col2 = pivot_col.BSPunsafe_access();
            for (int i{istart}; i < a.size(); i++) {
                //a.at(i) = A.at(i).BSPunsafe_access();
                for (int j{jstart}; j < a.at(i).size(); ++j) {
                    a.at(i).at(j) -=
                            raw_pivot_row2.at(j) * raw_pivot_col2.at(i);
                }
            }
            bsp_sync();
        }
    }

public:

    BSPlu(int n, int M, int N, unsigned int seed) : param_n{n}, param_M{M},
                                                    param_N{N}, rand{seed} {

    };

    void parallel_function() override {
        auto time = clock::now();

        int n = param_n;
        int M = param_M;
        int N = param_N;
        int s = bsp_pid() / N;
        int t = bsp_pid() % N;
        int nlr = nloc(M, s, n);
        int nlc = nloc(N, t, n);
        matrix<double> a(nlr);
        for (int i{0}; i < nlr; ++i) {
            a.at(i).resize(nlc);
            for (int j{0}; j < nlc; ++j) {
                a.at(i).at(j) = dist(rand);
            }
        }

#ifdef DEBUG
        std::ofstream inputwrite(std::string("lu_input.").append(std::to_string(bsp_pid())));
        for (const auto& row: a) {
            for (const auto& col: row) {
                inputwrite << col << " ";
            }
            inputwrite << std::endl;
        }
        inputwrite.close();
#endif

        std::vector<int> pi(nloc(bsp_nprocs(), bsp_pid(), n));
        auto timec = std::chrono::duration_cast<milliseconds>(
                clock::now() - time).count();
        std::cout << "Processor " << bsp_pid() << ": init took " << timec
                  << " ms." << std::endl;

        time = clock::now();
        bsplu(M, N, n, s, t, pi, a);
        timec = std::chrono::duration_cast<milliseconds>(
                clock::now() - time).count();
        if (bsp_pid() == 0)
            std::cout << "Processor " << bsp_pid() << ": LU took " << timec
                      << " ms." << std::endl;

#ifdef DEBUG
        std::ofstream outputwrite(std::string("lu_output.").append(std::to_string(bsp_pid())));
        for (const auto& row: a) {
            for (const auto& col: row) {
                outputwrite << col << " ";
            }
            outputwrite << std::endl;
        }
        outputwrite.close();

        std::ofstream piwrite(std::string("pi.").append(std::to_string(bsp_pid())));
        for (const auto& el: pi) {
            piwrite << el << std::endl;
        }
        piwrite.close();
#endif
    }
};

int main(int argc, char* argv[]) {
    if (argc < 4) {
        std::cerr << "Usage: " << argv[0] << " <n> <M> <N>" << std::endl;
        std::cerr << "       will start LU decomposition on a n by n matrix"
                  << std::endl;
        std::cerr << "       using M times N threads." << std::endl;
        return 1;
    }

    int n = std::stoi(argv[1]);
    int M = std::stoi(argv[2]);
    int N = std::stoi(argv[3]);

    std::random_device rd;
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (int i{0}; i < M * N; ++i) {
        nodes.emplace_back(std::make_unique<BSPlu>(n, M, N, rd()));
    }
    auto t0 = std::chrono::high_resolution_clock::now();
    auto f0 = [&t0]() {
        t0 = std::chrono::high_resolution_clock::now();
    };

    auto f1 = [&t0]() {
        auto t1 = std::chrono::high_resolution_clock::now();
        auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(
                t1 - t0);
        std::cout << "Main part took " << diff.count() << " ms." << std::endl;
    };

    bsp_program computation(std::move(nodes), f0, f1);
    computation.start();
    return 0;
}