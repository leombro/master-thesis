#include <bsp_program.hpp>

struct BSPinprod : public bsp_node {

    int problem_size;

    explicit BSPinprod(int n) : problem_size{n} {};

    inline int nloc(int p, int s, int n) const {
        return (n + p - s - 1) / p;
    }

    double bspip(int p, int s, int n,
                 const std::vector<double>& x,
                 const std::vector<double>& y) {

        auto Inprod = get_empty_array<double>(p);

        double inprod = 0.0;

        for (int i{0}; i < nloc(p, s, n); ++i) {
            inprod += x.at(i) * y.at(i);
        }
        for (int t{0}; t < p; ++t) {
            Inprod.bsp_put(inprod, t, s);
        }
        bsp_sync();

        const auto& Inprod_arr = Inprod.BSPunsafe_access();
        double alpha = 0.0;
        for (int t{0}; t < p; ++t) {
            alpha += Inprod_arr.at(t);
        }
        return alpha;
    }

    void parallel_function() override {
        int n = problem_size;
        int p = bsp_nprocs();
        int s = bsp_pid();
        int nl = nloc(p, s, n);

        std::vector<double> x(nl);

        for (int i{0}; i < nl; ++i) {
            x[i] = i * p + s + 1;
        }

        bsp_sync();
        std::cout << "taking time 1" << std::endl;
        auto t1 = std::chrono::high_resolution_clock::now();
        double alpha = bspip(p, s, n, x, x);
        auto t2 = std::chrono::high_resolution_clock::now();
        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                t2 - t1);
        if (s == 0) {
            std::cout << "Processor " << s << ": sum of squares up to "
                      << n << "*" << n << " is " << alpha << std::endl;
            std::cout << "Processor " << s << ": local time taken is "
                      << time.count()
                      << std::endl;
        }

    }
};

int main(int argc, char* argv[]) {
    if (argc == 1) {
        std::cerr << "Usage: " << argv[0] << " <P> (n)" << std::endl;
        std::cerr << "       where <P> is the number of processors used"
                  << std::endl;
        std::cerr << "       and (n) is the size of input vector, "
                  << "optional (default is 500000000)" << std::endl;
        return 1;
    }
    int p = std::stoi(argv[1]);
    int n = (argc >= 3) ? std::stoi(argv[2]) : 500000000;
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (int i{0}; i < p; ++i) {
        nodes.emplace_back(std::make_unique<BSPinprod>(n));
    }
    auto checksum = [n]() {
        auto dn = static_cast<double>(n);
        dn *= n + 1.0;
        dn *= 2.0 * n + 1.0;
        dn /= 6.0;
        std::cout << "Checksum: " << dn << std::endl;
    };

    bsp_program computation(std::move(nodes), nullptr, checksum);
    computation.start();
    return 0;
}