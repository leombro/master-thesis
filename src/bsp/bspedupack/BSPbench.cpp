#include <array>
#include <cmath>
#include <bsp_program.hpp>

struct bspbench : public bsp_node {

    int maxH, maxN, niters;

    const double MEGA = 1000000.0;

    bspbench(int _maxH, int _maxN, int _niters) :
            maxH{_maxH}, maxN{_maxN}, niters{_niters} {};

    std::array<double, 2>
    leastsquares(int h0, int h1, const std::vector<double>& t) const {
        std::array<double, 2> ret{};
        auto nh = static_cast<double>(h1 - h0 + 1);

        double sumt = 0.0;
        double sumth = 0.0;

        for (int h = h0; h <= h1; ++h) {
            sumt += t[h];
            sumth += t[h] * h;
        }
        double sumh = static_cast<double>(h1 * h1 - h0 * h0 + h1 + h0) / 2;
        double sumhh = static_cast<double>(h1 * (h1 + 1) * (2 * h1 + 1) -
                                           (h0 - 1) * h0 * (2 * h0 - 1)) / 6;

        if (std::abs(nh) > std::abs(sumh)) {
            double a = sumh / nh;
            ret[0] = (sumth - a * sumt) / (sumhh - a * sumh);
            ret[1] = (sumt - sumh * ret[0]) / nh;
        } else {
            double a = nh / sumh;
            ret[0] = (sumt - a * sumth) / (sumh - a * sumhh);
            ret[1] = (sumth - sumhh * ret[0]) / sumh;
        }

        return ret;
    }

    void parallel_function() override {

        double r = 0.0;

        std::vector<double> x(maxN);
        std::vector<double> y(maxN);
        std::vector<double> z(maxN);

        std::vector<int> destproc(maxH);
        std::vector<int> destindex(maxH);
        std::vector<double> src(maxH);

        std::vector<double> t(maxH + 1);

        int p = bsp_nprocs();
        int s = bsp_pid();

        bsp_array<double> Time = get_empty_array<double>(p);
        bsp_array<double> Dest = get_empty_array<double>(2 * maxH + p);

        for (int n = 1; n <= maxN; n *= 2) {

            double alpha = 1.0 / 3.0;
            double beta = 4.0 / 9.0;
            for (int i = 0; i < n; ++i) {
                z[i] = x[i] = y[i] = i;
            }

            auto time0 = std::chrono::high_resolution_clock::now();
            for (int iter = 0; iter < niters; ++iter) {
                for (int i = 0; i < n; ++i) {
                    y[i] += alpha * x[i];
                }
                for (int i = 0; i < n; ++i) {
                    z[i] -= beta * x[i];
                }
            }
            auto time1 = std::chrono::high_resolution_clock::now();
            auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                    time1 - time0).count() / 1000.0;
            Time.bsp_put(time, 0, s);
            bsp_sync();

            if (s == 0) {
                auto time_arr = Time.BSPunsafe_access();
                double mintime = time_arr[0];
                double maxtime = time_arr[0];
                for (int s1 = 1; s1 < p; ++s1) {
                    mintime = std::min(mintime, time_arr[s1]);
                    maxtime = std::max(maxtime, time_arr[s1]);
                }
                if (mintime > 0.0) {
                    int nflops = 4 * niters * n;
                    r = 0.0;
                    for (int s1 = 0; s1 < p; ++s1) {
                        r += static_cast<double>(nflops) / time_arr[s1];
                    }
                    r /= static_cast<double>(p);
                    std::cout << "n= "
                              << n << " min= "
                              << nflops / (maxtime * MEGA) << " max= "
                              << nflops / (mintime * MEGA) << " av= "
                              << r / MEGA << " Mflop/s" << std::endl;
                    std::cout << " fool=" << y[n - 1] + z[n - 1] << std::endl;
                } else {
                    std::cout << "minimum time is 0" << std::endl;
                }
            }
        }

        for (int h = 0; h <= maxH; ++h) {
            for (int i = 0; i < h; ++i) {
                src[i] = i;
                if (p == 1) {
                    destproc[i] = 0;
                    destindex[i] = i;
                } else {
                    destproc[i] = (s + 1 + i % (p - 1)) % p;
                    destindex[i] = s + (i / (p - 1)) * p;
                }
            }

            auto time0 = std::chrono::high_resolution_clock::now();
            for (int iter = 0; iter < niters; ++iter) {
                for (int i = 0; i < h; i++) {
                    Dest.bsp_put(src[i], destproc[i], destindex[i]);
                }
                bsp_sync();
            }
            auto time1 = std::chrono::high_resolution_clock::now();
            auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                    time1 - time0).count() / 1000.0;

            if (s == 0) {
                t[h] = (time * r) / static_cast<double>(niters);
                std::cout << "Time of " << h << "-relation= "
                          << time / niters << " sec= " << t[h] << " flops"
                          << std::endl;
            }
        }

        if (s == 0) {
            auto temp = leastsquares(0, p, t);
            std::cout << "Range h=0 to p: g= " << temp[0] << ", l= " << temp[1]
                      << std::endl;
            temp = leastsquares(p, maxH, t);
            std::cout << "Range h=p to HMAX: g= " << temp[0] << ", l= "
                      << temp[1] << std::endl;

            std::cout << "The bottom line for this BSP computer is:"
                      << std::endl;
            std::cout << "p= " << p << ", r= " << r / MEGA << " Mflop/s, g= "
                      << temp[0] << ", l= " << temp[1] << std::endl;

        }
    }

};

int main(int argc, char* argv[]) {

    int nprocs, maxh, maxn, niters;

    if (argc == 1) {
        std::cerr << "Usage: " << argv[0] << " <P> (NITERS) (MAXN) (MAXH)"
                  << std::endl;
        std::cerr << "<..> are obligatory parameters" << std::endl;
        std::cerr << "(..) are optional" << std::endl;
        return -1;
    }

    nprocs = std::stoi(argv[1]);

    if (argc >= 3) {
        niters = std::stoi(argv[2]);
    } else {
        niters = 10000;
    }

    if (argc >= 4) {
        maxn = std::stoi(argv[3]);
    } else {
        maxn = 1024;
    }

    if (argc >= 5) {
        maxh = std::stoi(argv[4]);
    } else {
        maxh = 128;
    }

    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (size_t i{0}; i < nprocs; ++i) {
        nodes.push_back(std::make_unique<bspbench>(maxh, maxn, niters));
    }
    bsp_program mbsp(std::move(nodes));
    mbsp.start();
}