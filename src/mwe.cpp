#include <iostream>
#include <iterator>
#include <algorithm>
#include <thread>
#include "bsp_program.hpp"

struct my_bsp_comp : public bsp_node {
    void parallel_function() override {
        int s = bsp_pid();
        int n = bsp_nprocs();
        auto v1 = get_variable<int>(10);
        v1.bsp_put(s, (s + 1) % n);
        bsp_sync();
        std::vector<int> init{s, s, s, s};
        auto a1 = get_array<int>(init);
        a1.bsp_put(init, (s + 1) % n, 0, 2, 2);
        bsp_sync();
        auto a = a1.bsp_direct_get(s);
        std::this_thread::sleep_for(std::chrono::seconds(s));
        std::cout << s << ": v1 is " << v1.bsp_direct_get(s) << std::endl;
        std::cout << s << ": a1 is ";
        std::copy(a.begin(),
                  a.end(),
                  std::ostream_iterator<int>(std::cout, " "));
        std::cout << std::endl;
    }
};

int main() {
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (int i{0}; i < 2; ++i) {
        nodes.push_back(std::make_unique<my_bsp_comp>());
    }
    auto pre_fun = []() {
        std::cout << "Before BSP computation" << std::endl;
    };
    auto post_fun = []() {
        std::cout << "After BSP computation" << std::endl;
    };
    bsp_program computation(std::move(nodes), pre_fun, post_fun);
    computation.start();
    return 0;
}