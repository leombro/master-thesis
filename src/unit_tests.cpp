#include <bsp_program.hpp>

static bool ok = true;
static int total_tests = 35;

static int mod(int k, int n) {
    return ((k %= n) < 0) ? k + n : k;
}

static void assertint(int a, int b, int id, int line) {
    std::string s;
    if (a != b) {
        s += "Node " + std::to_string(id) + ": mismatch at line " +
             std::to_string(line)
             + " (expected " + std::to_string(b) + ", obtained " +
             std::to_string(a) + ")";
        std::cerr << s << std::endl;
        ok = false;
    }
}

static void assertint(int a, int b, int c, int id, int line) {
    std::string s;
    if (a != b && a != c) {
        s += "Node " + std::to_string(id) + ": mismatch at line " +
             std::to_string(line)
             + " (expected " + std::to_string(b) + " or " + std::to_string(c) +
             ", obtained " + std::to_string(a) + ")";
        std::cerr << s << std::endl;
        ok = false;
    }
}

static void
assertvec(const std::vector<int>& a, const std::vector<int>& b, int id,
          int line) {
    std::string s;
    auto printvect = [](const std::vector<int>& v) {
        std::string s{"["};
        for (const auto& el: v) {
            s += std::to_string(el) + " ";
        }
        s.pop_back();
        s += "]";
        return s;
    };
    if (a != b) {
        s += "Node " + std::to_string(id) + ": mismatch at line " +
             std::to_string(line)
             + " (expected " + printvect(b) + ", obtained " + printvect(a) +
             ")";
        std::cerr << s << std::endl;
        ok = false;
    }
}

static void assertvec(const std::vector<int>& a, const std::vector<int>& b,
                      const std::vector<int>& c, int id, int line) {
    std::string s;
    auto printvect = [](const std::vector<int>& v) {
        std::string s{"["};
        for (const auto& el: v) {
            s += std::to_string(el) + " ";
        }
        s.pop_back();
        s += "]";
        return s;
    };
    if (a != b && a != c) {
        s += "Node " + std::to_string(id) + ": mismatch at line " +
             std::to_string(line)
             + " (expected " + printvect(b) + " or " + printvect(c) +
             ", obtained " + printvect(a) + ")";
        std::cerr << s << std::endl;
        ok = false;
    }
}

struct unit_tests : public bsp_node {

    void parallel_function() override {

        auto endfun = [this](bool end = false) {
            static int passed_tests = 0;
            if (end) {
                std::cout << "Number of tests executed successfully: "
                          << passed_tests << "/" << total_tests << std::endl;
                if (passed_tests == total_tests)
                    std::cout << "ALL TESTS COMPLETED SUCCESSFULLY!"
                              << std::endl;
                else
                    std::cerr
                            << "Some tests failed, refer to the logs for more information"
                            << std::endl;
            } else {
                if (ok) {
                    passed_tests++;
                    std::cout << "Test passed";
                } else std::cout << "Test failed";
                std::cout << " (" << passed_tests << "/" << total_tests << ")"
                          << std::endl;
                ok = true;
            }
        };


        if (bsp_pid() == 0) {
            std::cout
                    << "--------------------|  PART 1 : VARIABLES (SEQ) |-------------------"
                    << std::endl;
            std::cout
                    << "------------|  Test 1: bsp_put(const T& elem, int id)  |------------"
                    << std::endl;
        }

        bsp_variable<int> v1 = get_variable(-1);

        v1.bsp_put(bsp_pid(), bsp_pid());

        bsp_sync();

        assertint(v1.BSPunsafe_access(), bsp_pid(), bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--------|  Test 2 : bsp_put(const bsp_variable<T>& other)  |--------"
                    << std::endl;
        }

        bsp_variable<int> v2 = get_variable(-1);

        v2.bsp_put(v1);

        bsp_sync();

        assertint(v2.BSPunsafe_access(), bsp_pid(), bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "----|  Test 3 : bsp_put(const bsp_variable<T>& other, int id)  |----"
                    << std::endl;
        }

        bsp_variable<int> v3 = get_variable(-1);

        v3.bsp_put(v2, mod((bsp_pid() + 1), bsp_nprocs()));
        bsp_sync();

        assertint(v3.BSPunsafe_access(), mod((bsp_pid() - 1), bsp_nprocs()),
                  bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "---------------|  Test 4: bsp_put(int destination)  |---------------"
                    << std::endl;
        }

        bsp_variable<int> v4 = get_variable(bsp_pid());

        int nextnode = mod(bsp_pid() + 1, bsp_nprocs());
        v4.bsp_put(nextnode);
        bsp_sync();

        assertint(v4.BSPunsafe_access(), mod((bsp_pid() - 1), bsp_nprocs()),
                  bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "---------------|  Test 5: bsp_get(int destination)  |---------------"
                    << std::endl;
        }

        bsp_variable<int> v5 = get_variable(bsp_pid());
        bsp_sync(); // needed to make sure all nodes have time to create the variable

        v5.bsp_get(nextnode);
        bsp_sync();

        assertint(v5.BSPunsafe_access(), nextnode, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--------------------|  PART 2 : VARIABLES (PAR) |-------------------"
                    << std::endl;
            std::cout
                    << "------------|  Test 6: bsp_put(const T& elem, int id)  |------------"
                    << std::endl;
        }

        bsp_variable<int> v6 = get_variable(bsp_pid());

        if (bsp_pid() != 0) v6.bsp_put(bsp_pid(), 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertint(v6.BSPunsafe_access(), 1, 2, bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "----|  Test 7 : bsp_put(const bsp_variable<T>& other, int id)  |----"
                    << std::endl;
        }

        bsp_variable<int> v7 = get_variable(-1);
        if (bsp_pid() != 0) v7.bsp_put(v6, 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertint(v7.BSPunsafe_access(), 1, 2, bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "---------------|  Test 8: bsp_put(int destination)  |---------------"
                    << std::endl;
        }

        bsp_variable<int> v8 = get_variable(bsp_pid());
        if (bsp_pid() != 0) v8.bsp_put(0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertint(v8.BSPunsafe_access(), 1, 2, bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "----------------------|  PART 3: ARRAYS (SEQ) |---------------------"
                    << std::endl;
            std::cout
                    << "------------| Test 9 : bsp_put(const T& elem, int pos) |------------"
                    << std::endl;
        }

        std::vector<int> base_array{9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        std::vector<int> compare_array1{bsp_pid(), 8, 7, 6, 5, 4, 3, 2, 1, 0};

        bsp_array<int> a1 = get_array(base_array);
        a1.bsp_put(bsp_pid(), 0);
        bsp_sync();

        assertvec(a1.BSPunsafe_access(), compare_array1, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "----|  Test 10: bsp_put(const bsp_variable<T>& elem, int pos)  |----"
                    << std::endl;
        }

        bsp_array<int> a2 = get_array(base_array);
        bsp_variable<int> v9 = get_variable(bsp_pid());
        a2.bsp_put(v9, 0);
        bsp_sync();

        assertvec(a2.BSPunsafe_access(), compare_array1, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "---------|  Test 11 : bsp_put(T elem, int dest, int pos)  |---------"
                    << std::endl;
        }

        int prevnode = mod(bsp_pid() - 1, bsp_nprocs());
        std::vector<int> compare_array2{prevnode, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        bsp_array<int> a3 = get_array(base_array);

        a3.bsp_put(bsp_pid(), nextnode, 0);
        bsp_sync();

        assertvec(a3.BSPunsafe_access(), compare_array2, bsp_pid(), __LINE__);
        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--|  Test 12 : bsp_put(bsp_variable<T> elem, int dest, int pos)  |--"
                    << std::endl;
        }

        bsp_array<int> a4 = get_array(base_array);
        bsp_variable<int> v10 = get_variable(bsp_pid());
        a4.bsp_put(v10, nextnode, 0);
        bsp_sync();

        assertvec(a4.BSPunsafe_access(), compare_array2, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "------------|  Test 13 : bsp_put(std::vector<T> other) |------------"
                    << std::endl;

        }

        std::vector<int> replace_array{bsp_pid(), 1, 2, 3, 4};
        bsp_array<int> a5 = get_array(base_array);

        a5.bsp_put(replace_array);
        bsp_sync();

        assertvec(a5.BSPunsafe_access(), replace_array, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "-------------|  Test 14 : bsp_put(bsp_array<T> other) |-------------"
                    << std::endl;
        }

        bsp_array<int> a6 = get_array(base_array);
        bsp_array<int> a7 = get_array(replace_array);

        a6.bsp_put(a7);
        bsp_sync();

        assertvec(a6.BSPunsafe_access(), replace_array, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "-------|  Test 15 : bsp_put(std::vector<T> other, int dest) |-------"
                    << std::endl;
        }

        auto control_array1 = std::vector<int>{prevnode, 1, 2, 3, 4};

        bsp_array<int> a8 = get_array(base_array);

        a8.bsp_put(replace_array, nextnode);
        bsp_sync();

        assertvec(a8.BSPunsafe_access(), control_array1, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--------|  Test 16 : bsp_put(bsp_array<T> other, int dest) |--------"
                    << std::endl;
        }

        bsp_array<int> a9 = get_array(base_array);
        bsp_array<int> a10 = get_array(replace_array);

        a9.bsp_put(a10, nextnode);
        bsp_sync();

        assertvec(a9.BSPunsafe_access(), control_array1, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "------|  Test 17 : bsp_put(vector<T>, src_off, dst_off, len) |------"
                    << std::endl;
        }

        std::vector<int> substitution{0, 1, 2, 3, 4};
        for (auto& el: substitution) el += bsp_pid();
        std::vector<int> control_array2(base_array);
        std::copy_n(substitution.begin() + 1, 3, control_array2.begin() +
                                                 2); // {9, n+1, n+2, n+3, 5, 4, 3, 2, 1, 0}

        bsp_array<int> a11 = get_array(base_array);
        a11.bsp_put(substitution, 1, 2, 3);
        bsp_sync();

        assertvec(a11.BSPunsafe_access(), control_array2, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "-----|  Test 18: bsp_put(bsp_array<T>, src_off, dst_off, len) |-----"
                    << std::endl;
        }

        bsp_array<int> a12 = get_array(base_array);
        bsp_array<int> a13 = get_array(substitution);

        a12.bsp_put(a13, 1, 2, 3);
        bsp_sync();

        assertvec(a12.BSPunsafe_access(), control_array2, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "---|  Test 19 : bsp_put(vector<T>, dest, src_off, dst_off, len) |---"
                    << std::endl;
        }

        std::vector<int> substitution2{0, 1, 2, 3, 4};
        for (auto& el: substitution2) el += prevnode;
        std::vector<int> control_array3(base_array);
        std::copy_n(substitution2.begin() + 1, 3, control_array3.begin() + 2);

        bsp_array<int> a14 = get_array(base_array);
        a14.bsp_put(substitution, nextnode, 1, 2, 3);
        bsp_sync();

        assertvec(a14.BSPunsafe_access(), control_array3, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--|  Test 20: bsp_put(bsp_array<T>, dest, src_off, dst_off, len) |--"
                    << std::endl;
        }

        bsp_array<int> a15 = get_array(base_array);
        bsp_array<int> a16 = get_array(substitution);

        a15.bsp_put(a16, nextnode, 1, 2, 3);
        bsp_sync();

        assertvec(a15.BSPunsafe_access(), control_array3, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--------------|  Test 21 : bsp_get(int destination)  |--------------"
                    << std::endl;
        }

        std::vector<int> fillpids(5, bsp_pid());
        bsp_array<int> a17 = get_array(fillpids);
        bsp_sync();

        a17.bsp_get(nextnode);
        bsp_sync();

        assertvec(a17.BSPunsafe_access(), std::vector<int>(5, nextnode),
                  bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "----|  Test 22 : bsp_get(source, src_offs, dest_offs, length)  |----"
                    << std::endl;
        }

        std::vector<int> base_arr2{0, 1, 2, 3, 4};
        for (auto& el: base_arr2) el += bsp_pid();
        std::vector<int> control_array4{nextnode + 1, nextnode + 2,
                                        nextnode + 3, bsp_pid() + 3,
                                        bsp_pid() + 4};
        bsp_array<int> a18 = get_array(base_arr2);
        bsp_sync();

        a18.bsp_get(nextnode, 1, 0, 3);
        bsp_sync();

        assertvec(a18.BSPunsafe_access(), control_array4, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "----------------------|  PART 4: ARRAYS (PAR) |---------------------"
                    << std::endl;
            std::cout
                    << "--------|  Test 23&24: bsp_put(T elem, int dest, int pos)  |--------"
                    << std::endl;
            std::cout << "--------| Different positions" << std::endl;
        }

        bsp_array<int> a19 = get_array(base_array);

        a19.bsp_put(bsp_pid(), 0, bsp_pid());
        bsp_sync();

        auto compare_array5 = std::vector<int>{0, 1, 2, 6, 5, 4, 3, 2, 1, 0};
        if (bsp_pid() == 0) {
            assertvec(a19.BSPunsafe_access(), compare_array5, bsp_pid(),
                      __LINE__);
            endfun();
            std::cout << "--------| Same position" << std::endl;
        }

        bsp_array<int> a20 = get_array(base_array);

        if (bsp_pid() != 0) a20.bsp_put(bsp_pid(), 0, 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertint(a20.BSPunsafe_access().at(0), 1, 2, bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "-------|  Test 25&26 : bsp_put(bsp_variable<T>, dest, pos)  |-------"
                    << std::endl;
            std::cout << "-------| Different positions" << std::endl;
        }

        bsp_array<int> a21 = get_array(base_array);
        bsp_variable<int> v11 = get_variable(bsp_pid());

        a21.bsp_put(v11, 0, bsp_pid());
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a21.BSPunsafe_access(), compare_array5, bsp_pid(),
                      __LINE__);
            endfun();
            std::cout << "--------| Same position" << std::endl;
        }

        bsp_array<int> a22 = get_array(base_array);
        bsp_variable<int> v12 = get_variable(bsp_pid());

        if (bsp_pid() != 0) a22.bsp_put(v12, 0, 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertint(a22.BSPunsafe_access().at(0), 1, 2, bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "-------|  Test 27 : bsp_put(std::vector<T> other, int dest) |-------"
                    << std::endl;
        }

        bsp_array<int> a23 = get_empty_array<int>(5);

        if (bsp_pid() != 0) a23.bsp_put(std::vector<int>(5, bsp_pid()), 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a23.BSPunsafe_access(), std::vector<int>(5, 1),
                      std::vector<int>(5, 2), bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "--------|  Test 28 : bsp_put(bsp_array<T> other, int dest) |--------"
                    << std::endl;
        }

        bsp_array<int> a24 = get_empty_array<int>(5);
        bsp_array<int> a25 = get_array(std::vector<int>(5, bsp_pid()));

        if (bsp_pid() != 0) a24.bsp_put(a25, 0);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a24.BSPunsafe_access(), std::vector<int>(5, 1),
                      std::vector<int>(5, 2), bsp_pid(), __LINE__);
            endfun();
            std::cout
                    << "--|  Test 29&30: bsp_put(vector<T>, dest, src_off, dst_off, len) |--"
                    << std::endl;
            std::cout << "---| Separate positions" << std::endl;
        }

        const std::vector<int>& confront1{9, 1, 2, 3, 5, 4, 3, 98, 99, 0};
        const std::vector<int>& subst1{0, 1, 2, 3, 4, 5};
        const std::vector<int>& subst2{95, 96, 97, 98, 99};

        bsp_array<int> a26 = get_array(base_array);

        if (bsp_pid() == 1) a26.bsp_put(subst1, 0, 1, 1, 3);
        if (bsp_pid() == 2) a26.bsp_put(subst2, 0, 3, 7, 2);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a26.BSPunsafe_access(), confront1, bsp_pid(), __LINE__);
            endfun();
            std::cout << "---| Overlapping positions" << std::endl;
        }

        const std::vector<int>& confront2{9, 8, 7, 1, 2, 3, 4, 98, 99, 0};
        const std::vector<int>& confront3{9, 8, 7, 1, 2, 3, 97, 98, 99, 0};

        bsp_array<int> a27 = get_array(base_array);

        if (bsp_pid() == 1) a27.bsp_put(subst1, 0, 1, 3, 4);
        if (bsp_pid() == 2) a27.bsp_put(subst2, 0, 2, 6, 3);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a27.BSPunsafe_access(), confront2, confront3, bsp_pid(),
                      __LINE__);
            endfun();
            std::cout
                    << "|  Test 31&32 : bsp_put(bsp_array<T>, dest, src_off, dst_off, len) |"
                    << std::endl;
            std::cout << "| Separate positions" << std::endl;
        }

        bsp_array<int> a28 = get_array(base_array);
        bsp_array<int> a29 = get_array(bsp_pid() > 1 ? subst2 : subst1);

        if (bsp_pid() == 1) a28.bsp_put(a29, 0, 1, 1, 3);
        if (bsp_pid() == 2) a28.bsp_put(a29, 0, 3, 7, 2);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a28.BSPunsafe_access(), confront1, bsp_pid(), __LINE__);
            endfun();
            std::cout << "| Overlapping positions" << std::endl;
        }

        bsp_array<int> a30 = get_array(base_array);
        bsp_array<int> a31 = get_array(bsp_pid() > 1 ? subst2 : subst1);

        if (bsp_pid() == 1) a30.bsp_put(a31, 0, 1, 3, 4);
        if (bsp_pid() == 2) a30.bsp_put(a31, 0, 2, 6, 3);
        bsp_sync();

        if (bsp_pid() == 0) {
            assertvec(a30.BSPunsafe_access(), confront2, confront3, bsp_pid(),
                      __LINE__);
            endfun();
            std::cout
                    << "----------------------|  PART 5 : DIRECT GETS |---------------------"
                    << std::endl;
            std::cout
                    << "---------|  Test 33 (var) : T bsp_direct_get(int source)  |---------"
                    << std::endl;
        }

        bsp_variable<int> v13 = get_variable(bsp_pid());
        bsp_sync();

        int dg1 = v13.bsp_direct_get(0);
        v13.bsp_put(bsp_pid(), 0);
        bsp_sync();

        assertint(dg1, 0, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "-----|  Test 34 (arr): T bsp_direct_get(int source, int pos)  |-----"
                    << std::endl;
        }

        bsp_array<int> a32 = get_array(base_array);
        bsp_sync();

        int dg2 = a32.bsp_direct_get(0, 3);
        a32.bsp_put(bsp_pid(), 0, 3);
        bsp_sync();

        assertint(dg2, 6, bsp_pid(), __LINE__);

        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "-----|  Test 35 (arr) : vector<T> bsp_direct_get(int source)  |-----"
                    << std::endl;
        }

        bsp_array<int> a33 = get_array(fillpids);
        bsp_sync();

        auto dg3 = a33.bsp_direct_get(0);
        a33.bsp_put(std::vector<int>(5, bsp_pid()));
        bsp_sync();

        assertvec(dg3, std::vector<int>(5, 0), bsp_pid(), __LINE__);
        if (bsp_pid() == 0) {
            endfun();
            std::cout
                    << "--------------------------|  TEST ENDED  |--------------------------"
                    << std::endl;
            endfun(true);
        }
    }

};

int main() {
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (size_t i{0}; i < 3; ++i) {
        nodes.push_back(std::make_unique<unit_tests>());
    }
    bsp_program mbsp(std::move(nodes));
    mbsp.start();
}
