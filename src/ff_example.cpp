//
// Created by Orlando Leombruni on 21/04/2020.
//

#include <iostream>
#include <iterator>
#include <algorithm>
#include "bsp_program.hpp"

struct ff_example : public bsp_node {
    void parallel_function() override {
        int s = bsp_pid();
        auto v1 = (const std::vector<long>*) fastflow_input;
        long count = 0;
        int portion_size = v1->size() / bsp_nprocs();
        for (int i{portion_size * s}; i < portion_size * (s + 1); ++i)
            count += v1->at(i);
        bsp_array<long> counts = get_empty_array<long>(bsp_nprocs());
        counts.bsp_put(count, 0, s);
        bsp_sync();
        if (s == 0) {
            long total = 0;
            for (long l: counts.BSPunsafe_access()) {
                total += l;
            }
            emit_output((void*) total);
        }
    }
};

struct generator : public ff::ff_node {
    void* svc(void* in) override {
        auto source = new std::vector<long>;
        for (int i{0}; i < 10; ++i) source->push_back(i);
        ff_send_out(source);
        return EOS;
    }
};

struct checker : public ff::ff_node {
    void* svc(void* in) override {
        if (in != GO_ON && in != EOS) {
            auto val = (long) in;
            if (val == 45) std::cout << "OK" << std::endl;
            else std::cout << "KO" << std::endl;
        }
        return GO_ON;
    }
};

int main() {
    std::vector<std::unique_ptr<bsp_node>> nodes;
    for (int i{0}; i < 2; ++i) {
        nodes.push_back(std::make_unique<ff_example>());
    }
    auto pre_fun = []() {
        std::cout << "Before BSP computation" << std::endl;
    };
    auto post_fun = []() {
        std::cout << "After BSP computation" << std::endl;
    };
    bsp_program computation(std::move(nodes), pre_fun, post_fun);
    generator g;
    checker c;

    ff::ff_Pipe<> pipe(g, computation, c);

    if (pipe.run_and_wait_end() < 0)
        std::cout << "Error in running pipe" << std::endl;
    return 0;
}