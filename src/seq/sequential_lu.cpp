#include <iostream>
#include <chrono>
#include "lib/src/linalg.h"

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0]
                  << " <n>" << std::endl;
        std::cerr
                << "       will start LU "
                << "decomposition on a n by n "
                << "matrix."
                << std::endl;
        return 1;
    }

    int n = std::stoi(argv[1]);

    alglib::real_2d_array a;
    alglib::integer_1d_array piv;

    a.setlength(n, n);
    piv.setlength(n);

    for (int i{0}; i < n; ++i) {
        for (int j{0}; j < n; ++j) {
            a[i][j] = (alglib::randomreal() *
                       65.536) - 32.768;
        }
    }

    auto t1 = std::chrono::
    high_resolution_clock::now();
    alglib::rmatrixlu(a, n, n, piv);
    auto t2 = std::chrono::
    high_resolution_clock::now();

    auto time = std::chrono::
    duration_cast<std::chrono::
    milliseconds>(
            t2 - t1);
    std::cout << "Sequential computation took "
              << time.count() << " ms."
              << std::endl;
}