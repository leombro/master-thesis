#include <string>
#include <chrono>
#include <iostream>

int main(int argc, char* argv[]) {

    if (argc < 2) return 1;

    int n = std::stoi(argv[1]);

    auto x = new double[n]();

    for (int i{0}; i < n; ++i) {
        x[i] = i + 1;
    }

    double inprod = 0.0;
    auto t1 = std::chrono::high_resolution_clock::now();
    for (int i{0}; i < n; ++i) {
        inprod += x[i] * x[i];
    }
    auto t2 = std::chrono::high_resolution_clock::now() - t1;
    std::cout << "Processor 0: sum of squares up to " << n << "*" << n << " is "
              << inprod << std::endl;
    std::cout << "Processor 0: local time taken is "
              << std::chrono::duration_cast<std::chrono::milliseconds>(
                      t2).count() << std::endl;

    auto dn = static_cast<double>(n);
    dn *= n + 1.0;
    dn *= 2.0 * n + 1.0;
    dn /= 6.0;
    std::cout << "Checksum: " << dn << std::endl;

    delete[] x;

    return 0;
}

