#include <complex>
#include <iostream>
#include <vector>
#include <chrono>
#include <cassert>

constexpr static double PI = 3.141592653589793;

void ufft(std::vector<double>& x, int offset, int n, bool sign,
          const std::vector<double>& w) {

    for (int k = 2; k <= n; k *= 2) {
        int nk = n / k;
        for (int r = 0; r < nk; ++r) {
            int rk = 2 * r * k;
            for (int j = 0; j < k; j += 2) {
                double wr = w[j * nk];
                double wi;
                if (sign) {
                    wi = w[j * nk + 1];
                } else {
                    wi = -w[j * nk + 1];
                }

                int j0 = rk + j + offset;
                int j1 = j0 + 1;
                int j2 = j0 + k;
                int j3 = j2 + 1;

                double taur = wr * x[j2] - wi * x[j3];
                double taui = wi * x[j2] + wr * x[j3];

                x[j2] = x[j0] - taur;
                x[j3] = x[j1] - taui;
                x[j0] += taur;
                x[j1] += taui;
            }
        }
    }
}

void ufft_init(int n, std::vector<double>& w) {
    assert(w.size() == n);

    if (n == 1) return;

    w[0] = 1.0;
    w[1] = 0.0;

    if (n == 4) {
        w[2] = 0.0;
        w[3] = -1.0;
    } else if (n >= 8) {
        double theta = -2.0 * PI / static_cast<double>(n);
        for (int j = 1; j <= n / 8; j++) {
            w[2 * j] = std::cos(j * theta);
            w[2 * j + 1] = std::sin(j * theta);
        }
        for (int j = 0; j < n / 8; j++) {
            int n4j = n / 4 - j;
            w[2 * n4j] = -w[2 * j + 1];
            w[2 * n4j + 1] = -w[2 * j];
        }
        for (int j = 1; j < n / 4; j++) {
            int n2j = n / 2 - j;
            w[2 * n2j] = -w[2 * j];
            w[2 * n2j + 1] = w[2 * j + 1];
        }
    }
}


void twiddle_init(int n, double alpha, const std::vector<int>& rho,
                  std::vector<double>& w, int offset) {
    double theta = -2.0 * PI * alpha / static_cast<double>(n);
    for (int j = 0; j < n; ++j) {
        double rt = static_cast<double>(rho[j]) * theta;
        w[offset + 2 * j] = std::cos(rt);
        w[offset + 2 * j + 1] = std::sin(rt);
    }
}

void permute(std::vector<double>& x, int n, const std::vector<int>& sigma) {
    assert(x.size() / 2 == sigma.size());

    for (int j = 0; j < n; ++j) {
        if (j < sigma[j]) {
            int j0 = 2 * j;
            int j1 = j0 + 1;
            int j2 = 2 * sigma[j];
            int j3 = j2 + 1;
            double tmpr = x[j0];
            double tmpi = x[j1];
            x[j0] = x[j2];
            x[j1] = x[j3];
            x[j2] = tmpr;
            x[j3] = tmpi;
        }
    }
}

void bitrev_init(std::vector<int>& rho) {
    int n = rho.size();

    auto binary_len = static_cast<int>(std::ceil(
            std::log(static_cast<double>(n)) / std::log(2.0)));
    std::vector<bool> bits(binary_len);
    std::vector<int> pwrs(binary_len);
    pwrs[0] = 1;
    for (int j = 1; j < binary_len; ++j) {
        pwrs[j] = pwrs[j - 1] * 2;
    }
    int j = 0;
    while (j < n - 1) {
        j++;
        int lastbit = 0;
        while (bits[lastbit]) {
            bits[lastbit] = false;
            lastbit++;
        }
        bits[lastbit] = true;
        int val = 0;
        for (int k = 0; k < binary_len; ++k) {
            if (bits[k]) {
                val += pwrs[binary_len - k - 1];
            }
        }
        rho[j] = val;
    }
}


void fft_init(int n, std::vector<double>& w,
              std::vector<double>& tw, std::vector<int>& rho_np) {
    bitrev_init(rho_np);
    ufft_init(n, w);

    twiddle_init(n, 0, rho_np, tw, 0);
}

static void
calcError(const std::vector<double>& xlocal, const std::vector<double>& xarr) {
    double error = 0.0;
    for (int c{0}; c < xlocal.size(); c++) {
        double lerror = std::abs(xlocal[c] - xarr[c]);
        error += lerror;
    }
    std::cout << "local error is "
              << (error / static_cast<double>(xlocal.size())) << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <N>" << std::endl;
        std::cerr << " where N is the problem size (power of 2)" << std::endl;
        return -1;
    }
    int n;
    n = std::stoi(argv[1]);
    std::vector<double> data(n);

    n /= 2;
    for (int i = 0; i < n; i += 2) {
        data[i] = static_cast<double>(i) / 2.0;
    }

    std::vector<double> old(data);

    std::vector<double> w(n);
    std::vector<double> tw(2 * n);
    std::vector<int> rho_np(n);

    auto t = std::chrono::high_resolution_clock::now();
    fft_init(n, w, tw, rho_np);
    // forward fft
    permute(data, n, rho_np);
    ufft(data, 0, n, true, w);

    // inverse fft
    permute(data, n, rho_np);
    ufft(data, 0, n, false, w);

    double ninv = 1.0 / static_cast<double>(n);
    for (int j = 0; j < 2 * n; ++j) {
        data[j] *= ninv;
    }

    calcError(data, old);
    auto t1 = std::chrono::high_resolution_clock::now();


    std::cout << "Sequential part took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(
                      t1 - t).count() << " ms." << std::endl;

    calcError(data, old);


    return 0;
}