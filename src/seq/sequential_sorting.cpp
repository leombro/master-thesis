#include <random>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <chrono>

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <N> (seed)" << std::endl;
        std::cerr << " where N is the problem size (power of 2)" << std::endl;
        std::cerr
                << "       seed is an optional seed for permutations (leave blank to randomize it)"
                << std::endl;
        return -1;
    }
    int n, s;
    n = std::stoi(argv[1]);
    s = (argc >= 3 ? std::stoi(argv[2]) : -1);
    auto t = std::chrono::high_resolution_clock::now();
    if (s == -1) {
        std::random_device x;
        s = x();
    }
    std::mt19937 mtw(s);
    std::vector<int> data(n);
    std::iota(data.begin(), data.end(), 0);
    std::shuffle(data.begin(), data.end(), mtw);

    int* dat2 = new int[n]();

    for (int i = 0; i < n; ++i) dat2[i] = data.at(i);

    auto t1 = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - t).count();
    std::cout << "Ended vector generation and shuffling (spent " << t1 << " ms)"
              <<
              "\nstarting sequential part" << std::endl;

    auto t2 = std::chrono::high_resolution_clock::now();

    std::sort(data.begin(), data.end());

    auto t3p = std::chrono::high_resolution_clock::now();

    std::sort(dat2, dat2 + n);

    auto t4 = std::chrono::high_resolution_clock::now();

    bool passed = true;
    for (int j{0}; j < n; j++) {
        if (data[j] != j) passed = false;
    }

    auto t3 = std::chrono::duration_cast<std::chrono::milliseconds>(
            t3p - t2).count();
    auto t4c = std::chrono::duration_cast<std::chrono::milliseconds>(
            t4 - t3p).count();

    std::cout << "Check " << (passed ? "passed" : "failed") << std::endl;
    std::cout << "Parallel part took " << t3 << " ms." << std::endl;
    std::cout << "Plain array part took " << t4c << " ms." << std::endl;

    delete[] dat2;

}