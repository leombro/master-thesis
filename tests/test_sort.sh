#!/bin/bash

if [ ! -d tests/logs ]
then
	mkdir tests/logs
fi
if [ ! -d tests/logs/sort ]
then
	mkdir tests/logs/sort
fi
for j in {1..10}
do
	echo "repetition $j"
	echo "test sequential"
	if [ ! -d tests/logs/sort/0 ]
	then
		mkdir tests/logs/sort/0
	fi
	bin/seq/sequential_sorting 33554432 | grep "Parallel" > tests/logs/sort/0/test-0-$j.log
	for i in 1 2 4 8 16 32 64
	do
		echo "test $i thread"
		if [ ! -d tests/logs/sort/$i ]
		then
			mkdir tests/logs/sort/$i
		fi
		bin/bsp/BSPpsrs 33554432 $i | grep "Parallel" > tests/logs/sort/$i/test-$i-$j.log
	done
done
for i in 0 1 2 4 8 16 32 64
do
	echo "computing aggregates for $i threads"
	cat tests/logs/sort/$i/*.log | awk -v var="$i" '{ print $4 "," var }' > tests/logs/sort/$i/aggregate.csv
done 
