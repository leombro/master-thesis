#!/bin/bash

if [ ! -d tests/logs ]
then
	mkdir tests/logs
fi
if [ ! -d tests/logs/fft ]
then
	mkdir tests/logs/fft
fi
for j in {1..10}
do
	echo "repetition $j"
	echo "test sequential"
	if [ ! -d tests/logs/fft/0 ]
	then
		mkdir tests/logs/fft/0
	fi
	bin/seq/sequential_fft 33554432 | grep "Sequential" > tests/logs/fft/0/test-0-$j.log
	for i in 1 2 4 8 16 32 64
	do
		echo "test $i thread"
		if [ ! -d tests/logs/fft/$i ]
		then
			mkdir tests/logs/fft/$i
		fi
		bin/bspedupack/BSPfft 33554432 $i | grep "Parallel" > tests/logs/fft/$i/test-$i-$j.log
	done
done
for i in 0 1 2 4 8 16 32 64
do
	echo "computing aggregates for $i threads"
	cat tests/logs/fft/$i/*.log | awk -v var="$i" '{ print $4 "," var }' > tests/logs/fft/$i/aggregate.csv
done 
