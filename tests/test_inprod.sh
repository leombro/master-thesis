#!/bin/bash

if [ ! -d tests/logs ]
then
	mkdir tests/logs
fi
if [ ! -d tests/logs/inprod ]
then
	mkdir tests/logs/inprod
fi
for j in {1..10}
do
	echo "repetition $j"
	echo "test sequential"
	if [ ! -d tests/logs/inprod/0 ]
	then
		mkdir tests/logs/inprod/0
	fi
	bin/seq/sequential_inprod 2000000000 | grep "taken" > tests/logs/inprod/0/test-0-$j.log
	for i in 1 2 4 8 16 32 64
	do
		echo "test $i thread"
		if [ ! -d tests/logs/inprod/$i ]
		then
			mkdir tests/logs/inprod/$i
		fi
		bin/bspedupack/BSPinprod $i 2000000000 | grep "taken" > tests/logs/inprod/$i/test-$i-$j.log
	done
done
for i in 0 1 2 4 8 16 32 64
do
	echo "computing aggregates for $i threads"
	cat tests/logs/inprod/$i/*.log | awk -v var="$i" '{ print $7 "," var }' > tests/logs/inprod/$i/aggregate.csv
done 
