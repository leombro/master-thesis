#!/bin/bash

if [ ! -d tests/logs ]
then
	mkdir tests/logs
fi
if [ ! -d tests/logs/lu ]
then
	mkdir tests/logs/lu
fi
for j in {1..10}
do
	echo "repetition $j"
	echo "test sequential"
	if [ ! -d tests/logs/lu/0 ]
	then
		mkdir tests/logs/lu/0
	fi
  bin/seq/sequential_lu 5000 | grep "Sequential computation took" > tests/logs/lu/0/test-0-$j.log
	for i in 1 2 4 8
	do
		for k in $i $((i*2))
		do
			l=$((i*k))
			if [ $l -le 64 ]
			then
			  echo "test $l = $i * $k thread"
			  if [ ! -d tests/logs/lu/$l ]
			  then
				  mkdir tests/logs/lu/$l
			  fi
			  bin/bspedupack/BSPlu 5000 $i $k | grep "LU took" > tests/logs/lu/$l/test-$l-$j.log
			fi
		done
	done
done
echo "computing aggregates for 0 threads"
cat tests/logs/lu/0/*.log | awk -v var="0" '{ print $4 "," var }' > tests/logs/lu/0/aggregate.csv
for i in 1 2 4 8 16 32 64
do
	echo "computing aggregates for $i threads"
	cat tests/logs/lu/$i/*.log | awk -v var="$i" '{ print $5 "," var }' > tests/logs/lu/$i/aggregate.csv
done 
