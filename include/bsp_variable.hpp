//
// Created by Orlando Leombruni on 23/09/2019.
//

#ifndef FF_BSP_BSP_VARIABLE_HPP
#define FF_BSP_BSP_VARIABLE_HPP

#include <type_traits>
#include "bsp_internals.hpp"

/**
 * A variable data structure, private to a single worker but with support for
 * communication with other similar entities.
 *
 * @tparam T type of the variable
 */
template<typename T>
class bsp_variable : public bsp_container {

    // The template type must be copy-constructible and copy-assignable
    static_assert(std::is_copy_constructible<T>::value &&
                  std::is_copy_assignable<T>::value,
                  "Type of bsp_variable must be copy-constructible "
                  "and copy-assignable");

private:

    // bsp_array can access private fields of this class
    template<typename E>
    friend class bsp_array;

    // bsp_communicator can access private fields of this class
    friend class bsp_communicator;

    /**
     * Pointer to the actual data element.
     */
    T* element;

    /**
     * Default constructor.
     */
    bsp_variable() = default;

    /**
     * Builds a bsp_variable object that holds a node's private copy of a shared
     * variable.
     * @param _ref ID of the shared variable
     * @param _hold ID of the requesting node
     * @param comm pointer to the communicator component
     * @param ptr pointer to the node's private copy
     */
    explicit bsp_variable(int _ref, int _hold, bsp_communicator* comm, T* ptr) :
            bsp_container(_ref, _hold, comm), element{ptr} {
    }

    /**
     * Returns the shared object type (in this case, a variable).
     * @return the \c vartype value for variables
     */
    vartype var_type() final {
        return vartype::variable;
    }

public:

    /**
     * Returns a copy of the node's private element of the shared variable.
     * @return a copy of the desired element
     */
    const T& get() {
        return *element;
    }


    // Replaces the element on another worker
    /**
     * Replaces another node's private element of the shared variable.
     * @param elem element to be copied
     * @param destination ID of the destination node
     */
    void bsp_put(const T& elem, int destination) {
        comm->variable_put<T>(reference, holder, destination, elem);
    }

    /**
     * Replaces this node's private element with this node's private element of
     * another shared variable.
     * @param other the handle to the other shared variable
     */
    void bsp_put(const bsp_variable<T>& other) {
        const auto& t = *(other.element);
        bsp_put(t, holder);
    }

    /**
     * Replaces another node's private element with this node's private
     * element of another shared variable.
     * @param other the handle to the other shared variable
     * @param destination ID of the destination node
     */
    void bsp_put(const bsp_variable<T>& other, int destination) {
        const auto& t = *(other.element);
        bsp_put(t, destination);
    }

    /**
     * Replaces another node's private element with this node's private
     * element of this shared variable.
     * @param destination ID of the destination node
     */
    void bsp_put(int destination) {
        comm->variable_put<T>(reference, holder, destination);
    }

    /**
     * Replaces this node's private element with another node's private
     * element of this shared variable.
     * @param source ID of the source node
     */
    void bsp_get(int source) override {
        comm->variable_put<T>(reference, source, holder);
    }

    /**
     * Returns a copy of a node's private element of this shared variable.
     * This method will return immediately, without waiting for a superstep
     * sync.
     * @param source ID of the source node
     * @return a copy of the desired element
     */
    T bsp_direct_get(int source) {
        return comm->variable_direct_get<T>(reference, source);
    }

    /**
     * Returns a handle to this node's private element of the shared variable.
     * The returned object can be modified at will, without waiting for
     * superstep syncs.
     * This function is <b>BSP unsafe</b>.
     * @return a reference to the node's private element of the shared variable
     */
    T& BSPunsafe_access() {
        return *element;
    }
};

#endif //FF_BSP_BSP_VARIABLE_HPP
