//
// Created by Orlando Leombruni on 19/02/2020.
//

#ifndef FF_BSP_STL_ALLOCATOR_HPP
#define FF_BSP_STL_ALLOCATOR_HPP

#include <ff/allocator.hpp>

namespace ff {

    /**
     * STL-compliant wrapper for FastFlow's custom allocator.
     * @tparam T the type of objects to be allocated or deallocated.
     */
    template<typename T>
    class FF_Allocator {
    public:
        using value_type = T;
        using propagate_on_container_move_assignment = std::true_type;
        using is_always_equal = std::true_type;

        /**
         * Default constructor.
         */
        FF_Allocator() noexcept = default;

        /**
         * Empty copy constructor.
         * @tparam U the type of objects of the other allocator object
         * @param other the other allocator object
         */
        template<class U>
        explicit FF_Allocator(const FF_Allocator<U>& other) noexcept {};

        /**
         * Allocates n * sizeof(T) bytes of uninitialized storage by calling
         * the FastFlow allocator instance's \c malloc function.
         * @param n the number of objects to allocate storage for
         * @return the pointer to the first newly-allocated object
         */
        value_type* allocate(std::size_t n) {
            return static_cast<value_type*>(FFAllocator::instance()
                    ->malloc(n * sizeof(value_type)));
        }

        /**
         * Deallocates the storage referenced by the pointer p, which must be
         * a pointer obtained by an earlier call to allocate().
         * @param ptr pointer to the object to deallocate
         */
        void deallocate(value_type* ptr, std::size_t) noexcept {
            FFAllocator::instance()->free(ptr);
        }
    };

    /**
     * Checks that two allocator objects can be considered the same.
     * @tparam T type of the first allocator
     * @tparam U type of the second allocator
     * @return true
     */
    template<class T, class U>
    bool
    operator==(const FF_Allocator<T>&, const FF_Allocator<U>&) { return true; }

    /**
     * Checks that two allocator objects can be considered different.
     * @tparam T type of the first allocator
     * @tparam U type of the second allocator
     * @return false
     */
    template<class T, class U>
    bool
    operator!=(const FF_Allocator<T>&, const FF_Allocator<U>&) { return false; }
}

#endif //FF_BSP_STL_ALLOCATOR_HPP
