//
// Created by Orlando Leombruni on 24/09/2019.
//

#ifndef FF_BSP_BSP_ARRAY_HPP
#define FF_BSP_BSP_ARRAY_HPP

#include <type_traits>
#include <vector>
#include "bsp_variable.hpp"

/**
 * An array structure, private to a single worker but with support
 * for communication with other similar entities.
 *
 * @tparam T type of the elements contained in the array.
 */
template<typename T>
class bsp_array : bsp_container {

    // The template type must be copy-constructible and copy-assignable
    static_assert(std::is_copy_constructible<T>::value &&
                  std::is_copy_assignable<T>::value,
                  "Type of elements of bsp_array must be copy-constructible "
                  " and copy-assignable");

private:

    // bsp_communicator can access private fields of this class
    friend class bsp_communicator;

    /**
     * Pointer to the actual vector data.
     */
    std::vector<T>* arr;

    /**
     * Default constructor.
     */
    bsp_array() = default;

    /**
     * Builds a bsp_array object that holds a node's private copy of a shared
     * array.
     *
     * @param _ref ID of the shared array
     * @param _hold ID of the requesting node
     * @param comm pointer to the communicator component
     * @param ptr pointer to the node's private copy
     */
    bsp_array(int _ref, int _hold, bsp_communicator* comm,
              std::vector<T>* ptr) :
            bsp_container(_ref, _hold, comm), arr{ptr} {
    }

    /**
     * Returns the shared object type (in this case, an array).
     * @return the \c vartype value for arrays
     */
    vartype var_type() final {
        return vartype::array;
    }

public:

    /**
     * Returns a copy of the element in position <tt>position</tt> of the
     * node's private copy of the shared array.
     * @param position position of the desired element
     * @return a copy the array element in the desired position
     */
    const T& get(int position) {
        return arr->at(position);
    }

    /**
     * Returns a copy of the node's private copy of the shared array.
     * @return a copy of the array
     */
    const std::vector<T>& get() {
        return *arr;
    }

    /**
     * Returns the size of the array.
     * @return the size of the array
     */
    int size() {
        return arr->size();
    }

    /**
     * Puts an element into the contained array in the desired
     * position. This function is <b>BSP unsafe</b>.
     * @param elem element to be copied
     * @param pos position in the array
     */
    void BSPunsafe_put(const T& elem, int pos) {
        arr->at(pos) = elem;
    }

    /**
     * Puts an element into the given position in this node's private copy of
     * the array.
     * @param elem element to be inserted
     * @param pos position in the array
     */
    void bsp_put(const T& elem, int pos) {
        bsp_put(elem, holder, pos);
    }

    /**
     * Puts an element (contained in a <tt>>bsp_variable</tt>) into the given
     * position in this node's private copy of the array.
     * @param elem \c bsp_variable that contains the element to be inserted
     * @param pos position in the array
     */
    void bsp_put(const bsp_variable<T>& elem, int pos) {
        const auto& t = *(elem.element);
        bsp_put(t, pos);
    }

    /**
     * Puts an element into the given position in another node's private copy of
     * the array.
     * @param elem element to be inserted
     * @param destination ID of the destination node
     * @param pos position in the array
     */
    void bsp_put(const T& elem, int destination, int pos) {
        comm->array_put(reference, holder, destination, pos, elem);
    }

    /**
     * Puts an element (contained in a <tt>>bsp_variable</tt>) into the given
     * position in this node's private copy of the array.
     * @param elem \c bsp_variable that contains the element to be inserted
     * @param destination ID of the destination node
     * @param pos position in the array
     */
    void bsp_put(const bsp_variable<T>& elem, int destination, int pos) {
        const auto& t = *(elem.element);
        bsp_put(t, destination, pos);
    }

    /**
     * Replaces the node's private copy of the array with the given vector.
     * @param array vector that will replace the current private copy of the
     * array
     */
    void bsp_put(const std::vector<T>& array) {
        comm->array_put(reference, holder, 0, holder, 0, 0, array);
    }

    /**
     * Replaces the node's private copy of the array with the vector
     * contained inside another <tt>bsp_array</tt>.
     * @param other <tt>bsp_array</tt> containing the vector that will
     * replace the current one
     */
    void bsp_put(const bsp_array<T>& other) {
        const auto& t = *(other.arr);
        bsp_put(t);
    }

    /**
     * Replaces another node's private copy of the array with the given vector.
     * @param array vector that will replace the current private copy of the
     * array
     * @param destination ID of the destination node
     */
    void bsp_put(const std::vector<T>& array, int destination) {
        comm->array_put(reference, holder, 0, destination, 0, 0, array);
    }

    /**
     * Replaces another node's private copy of the array with the vector
     * contained inside the given <tt>bsp_array</tt>.
     * @param other <tt>bsp_array</tt> containing the vector that will
     * replace the current private copy of the array
     * @param destination ID of the destination node
     */
    void bsp_put(const bsp_array<T>& other, int destination) {
        const auto& t = *(other.arr);
        bsp_put(t, destination);
    }

    /**
     * Replaces a portion of this node's private copy of the array with a
     * portion of the given vector.
     * @param array vector that contains the desired portion
     * @param src_offset starting position of the replacing portion
     * @param dest_offset starting position of the portion to be replaced
     * @param length length of the replacing portion
     */
    void bsp_put(const std::vector<T>& array, int src_offset, int dest_offset,
                 int length) {
        comm->array_put(reference, holder, src_offset, holder, dest_offset,
                        length, array);
    }

    /**
     * Replaces a portion of this node's private copy of the array with a
     * portion of the vector contained inside the given <tt>bsp_array</tt>.
     * @param other <tt>bsp_array</tt> containing the vector that contains
     * the desired portion
     * @param src_offset starting position of the replacing portion
     * @param dest_offset starting position of the portion to be replaced
     * @param length length of the replacing portion
     */
    void bsp_put(const bsp_array<T>& other, int src_offset, int dest_offset,
                 int length) {
        const auto& t = *(other.arr);
        bsp_put(t, src_offset, dest_offset, length);
    }

    /**
     * Replaces a portion of another node's private copy of the array with a
     * portion of the given vector.
     * @param array vector that contains the desired portion
     * @param destination ID of the destination node
     * @param src_offset starting position of the replacing portion
     * @param dest_offset starting position of the portion to be replaced
     * @param length length of the replacing portion
     */
    void bsp_put(const std::vector<T>& array, int destination, int src_offset,
                 int dest_offset, int length) {
        comm->array_put(reference, holder, src_offset, destination, dest_offset,
                        length, array);
    }

    /**
     * Replaces a portion of another node's private copy of the array with a
     * portion of the vector contained inside the given <tt>bsp_array</tt>.
     * @param other <tt>bsp_array</tt> containing the vector that contains
     * the desired portion
     * @param destination ID of the destination node
     * @param src_offset starting position of the replacing portion
     * @param dest_offset starting position of the portion to be replaced
     * @param length length of the replacing portion
     */
    void bsp_put(const bsp_array<T>& other, int destination, int src_offset,
                 int dest_offset, int length) {
        const auto& t = *(other.arr);
        bsp_put(t, destination, src_offset, dest_offset, length);
    }

    /**
     * Replaces the node's private copy of the array with another node's
     * private copy.
     * @param from ID of the source node
     */
    void bsp_get(int from) override {
        comm->array_get<T>(reference, from, 0, holder, 0, 0);
    }

    /**
     * Replaces a portion of the node's private copy of the array with a
     * portion of another node's private copy.
     * @param from ID of the source node
     * @param from_offset starting position of the replacing portion
     * @param to_offset starting position of the portion to be replaced
     * @param length length of the replacing portion
     */
    void bsp_get(int from, int from_offset, int to_offset, int length) {
        comm->array_get<T>(reference, from, from_offset, holder, to_offset,
                           length);
    }

    /**
     * Returns a copy of the element at the desired position from another node's
     * copy of the array. This method will return immediately, without
     * waiting for a superstep sync.
     * @param source ID of the source node
     * @param pos position of the desired element
     * @return the element of the source's private copy at the desired position
     */
    T bsp_direct_get(int source, int pos) {
        return comm->array_direct_get<T>(reference, source, pos);
    }

    /**
     * Returns a copy of another node's private array. This method will return
     * immediately, without waiting for a superstep sync.
     * @param source ID of the source node
     * @return tthe source's private copy of the shared array
     */
    std::vector<T> bsp_direct_get(int source) {
        return comm->array_direct_get<T>(reference, source);
    }

    /**
     * Returns a handle to this node's private copy of the shared array. The
     * returned object can be modified at will, without waiting for superstep
     * syncs.
     * This function is <b>BSP unsafe</b>.
     * @return a reference to the node's private copy of the array
     */
    std::vector<T>& BSPunsafe_access() {
        return *arr;
    }

};


#endif //FF_BSP_BSP_ARRAY_HPP
