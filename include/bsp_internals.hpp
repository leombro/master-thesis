//
// Created by Orlando Leombruni on 23/09/2019.
//

#ifndef FF_BSP_INTERNALS_HPP
#define FF_BSP_INTERNALS_HPP

#include <utility>
#include <vector>
#include <memory>
#include <shared_mutex>
#include <map>
#include <algorithm>
#include <atomic>
#include "stl_allocator.hpp"

#ifdef STL_ALLOC
    #define alloc(T) std::allocator<T>
#else
    #define alloc(T) ff::FF_Allocator<T>
#endif

/**
 * Contains definitions and class prototypes used by the communicator and
 * variable/array classes
 */

/**
 * Represents the type of a shared object (variable or array)
 */
typedef enum {
    array,
    variable
} vartype;

// Forward declaration
class bsp_communicator;

/**
 * Base class for BSP variables and arrays, i.e. private copies of shared
 * objects.
 */
class bsp_container {
protected:
    //! ID of the shared object
    int reference;
    //! ID of the node that owns this private copy
    int holder;
    //! Pointer to the communicator object
    bsp_communicator* comm;

    //! Method that will return the object's type (variable or array)
    virtual vartype var_type() = 0;

    /**
     * Base constructor for the class.
     * @param _reference ID of the shared object
     * @param holder_pid ID of the node that owns this private copy
     * @param _comm Pointer to the communicator object
     */
    bsp_container(int _reference, int holder_pid, bsp_communicator* _comm) :
            reference{_reference}, holder{holder_pid}, comm{_comm} {};

public:

    virtual void bsp_get(int) = 0;

};

// Forward declaration
template<typename T>
class bsp_variable;
// Forward declaration
template<typename T>
class bsp_array;

// Class declaration for the communicator
class bsp_communicator {
private:
    /**
     * Represent the possible types for a request object.
     */
    enum request_type {
        var_put,
        arr_put_el,
        arr_put,
        arr_get
    };

    /**
     * Representation of a put/get request in the communicator.
     */
    struct request {
        request_type t;
        int reference;
        int source;
        int destination;
        int src_offset;
        int dest_offset;
        int length;
        std::shared_ptr<void> element;

        request(request_type _t, int _ref, int _src, int _dest,
                int _srcof, int _dstof, int _len, std::shared_ptr<void> _el) :
                t{_t}, reference{_ref}, source{_src}, destination{_dest},
                src_offset{_srcof}, dest_offset{_dstof}, length{_len},
                element{std::move(_el)} {};
    };

    /**
     * Shared variable object in the shared memory.
     */
    struct inner_var {
        //! Vector of private copies of the variable
        std::vector<void*, alloc(void*)> element;
        //! Bookkeeping function needed to work with <tt>void*</tt>
        //! Replaces a variable with another value
        void (* swap)(void* el,
                      void* other);
        //! Bookkeeping function needed to work with <tt>void*</tt>
        //! Safely frees memory occupied by an element
        void (* free_el)(
                void* el);

        /**
         * Constructor for the shared variable object.
         * @param nprocs number of BSP nodes in the computation
         * @param swapfun pointer to function for element swapping
         * @param free_elfun pointer to function for safe delete
         */
        inner_var(int nprocs, void (* swapfun)(void*, void*),
                  void (* free_elfun)(void*)) : swap{swapfun},
                                                free_el{free_elfun} {
            element.resize(nprocs);
        }
    };

    /**
     * Shared array object in the shared memory.
     */
    struct inner_array {
        //! Vector of private copies of the array
        std::vector<void*, alloc(void*)> element;
        //! Bookkeeping function needed to work with <tt>void*</tt>
        //! Replaces an element of the a private copy of the array with another
        //! value
        void (* put)(void* el, void* toput,
                     int pos);
        //! Bookkeeping function needed to work with <tt>void*</tt>
        //! Replaces a portion of a private copy of the array
        void (* replace)(void* el, int srcof, int dstof, int len,
                         void* toput);
        //! Bookkeeping function needed to work with <tt>void*</tt>
        //! Safely frees memory occupied by an element
        void (* free_el)(
                void* el);

        /**
         * Constructor for the shared variable object.
         * @param nprocs number of BSP nodes in the computation
         * @param putfun pointer to function for element swapping
         * @param replacefun pointer to function for replacing a portion
         * @param free_elfun pointer to function for safe delete
         */
        inner_array(int nprocs, void (* putfun)(void*, void*, int),
                    void (* replacefun)(void*, int, int, int, void*),
                    void (* free_elfun)(void*)) :
                put{putfun}, replace{replacefun}, free_el{free_elfun} {
            element.resize(nprocs);
        }
    };

    /**
     * Hashes a string and a number according to the dbj2 hash.
     * @param s the string to be hashed
     * @param seed an initial value for the hashing function
     * @return a (hopefully) unique number that represents the input pair
     */
    static int get_hash(const char* s, int seed) {
        unsigned int hash = seed + 5381;
        while (*s) {
            hash = hash * 33 ^ (*s++);
        }
        return hash;
    }

    //! Number of BSP nodes
    int nprocs;
    //! Number of current superstep
    int generation = 1;

    //! Counts the variables requested by each node in this superstep
    std::map<std::string, int>* var_count;
    //! Counts the arrays requested by each node in this superstep
    std::map<std::string, int>* arr_count;

    //! Mutex for multiple readers-single writer access to the variables
    mutable std::shared_mutex var_mutex;
    //! Mutex for multiple readers-single writer access to the arrays
    mutable std::shared_mutex arr_mutex;

    //! Dictionary for quick retrieving of variables based on their hash
    std::map<int, int> variable_dict;
    //! Dictionary for quick retrieving of arrays based on their hash
    std::map<int, int> array_dict;

    //! Container that stores variables
    std::vector<inner_var> variables_storage;
    //! Container that stores arrays
    std::vector<inner_array> arrays_storage;

    //! Array of mutexes to access request queues
    std::mutex* mutexes;
    //! Requests of variable/array modifications submitted during this superstep
    std::vector<request, alloc(request)>* requests;

    //! Counts the nodes that requested a sync during this superstep
    std::atomic_int process_count{0};

    //! Pointer to the input token received by the FastFlow node
    const void* fastflow_input = nullptr;

public:

    /**
     * Constructor for the communicator.
     * @param _nprocs number of BSP nodes in this computation.
     */
    explicit bsp_communicator(int _nprocs) : nprocs{_nprocs} {
        var_count = new std::map<std::string, int>[_nprocs]();
        arr_count = new std::map<std::string, int>[_nprocs]();
        mutexes = new std::mutex[_nprocs]();
        requests = new std::vector<request, alloc(request)>[_nprocs]();
        inner_var invar{nprocs, [](void*, void*) {}, [](void*) {}};
        variables_storage.push_back(invar);
    };

    // Refer to bsp_communicator.hpp

    void set_fastflow_input(const void*);

    const void* get_fastflow_input();

    template<typename T>
    void variable_put(int, int, int, const T&);

    template<typename T>
    void variable_put(int, int, int);

    template<typename T>
    T variable_direct_get(int, int);

    template<typename T>
    void array_put(int, int, int, int, const T&);

    template<typename T>
    void array_put(int, int, int, int, int, int, const std::vector<T>&);

    template<typename T>
    void array_get(int, int, int, int, int, int);

    template<typename T>
    T array_direct_get(int, int, int);

    template<typename T>
    std::vector<T> array_direct_get(int, int);

    template<typename T>
    bsp_variable<T> get_variable(int holder, T* initial_val);

    template<typename T>
    bsp_array<T>
    get_array(int holder, std::vector<T>* initial_arr, bool to_delete = true);

    void process_requests(int id);

    void end();

};

#endif //FF_BSP_BSP_VARIABLE_INTERNAL_HPP
