//
// Created by Orlando Leombruni on 24/09/2019.
//

#ifndef FF_BSP_BSP_COMMUNICATOR_HPP
#define FF_BSP_BSP_COMMUNICATOR_HPP

#include <set>
#include <iostream>
#include "bsp_array.hpp"

#ifdef STL_ALLOC
#define m_shared_ptr(U, what) \
    std::allocate_shared<U, std::llocator<U>> (std::allocator<U>(), (what))
#define alloc(U) std::allocator<U>
#else
#define m_shared_ptr(U, what) \
    std::allocate_shared<U, ff::FF_Allocator<U>> (ff::FF_Allocator<U>(), (what))
#define alloc(U) ff::FF_Allocator<U>
#endif

/**
 * Implementation of the communicator entity.
 */

// Requests management

/**
 * Inserts a request for replacing a node's private copy of a shared variable
 * with the provided value.
 * @tparam T the type of the variable
 * @param what ID of the variable to be modified
 * @param source ID of the node that performs the request
 * @param destination ID of the node that holds the private copy of the
 * variable that will be modified
 * @param element the value to be copied inside the destination
 */
template<typename T>
void bsp_communicator::variable_put(int what, int source, int destination,
                                    const T& element) {
    if (what == 0) return;
    mutexes[destination].lock();
    requests[destination].emplace_back(request_type::var_put, what, source,
                                       destination, 0, 0, 0,
                                       m_shared_ptr(T, element));
    mutexes[destination].unlock();
}

/**
 * Inserts a request for replacing a node's private copy of a shared variable
 * with the requestor's private value of the same shared variable.
 * @tparam T the type of the variable
 * @param what ID of the variable to be modified
 * @param source ID of the node that performs the request
 * @param dest ID of the node that holds the private copy of the
 * variable that will be modified
 */
template<typename T>
void bsp_communicator::variable_put(int what, int source, int dest) {
    if (what == 0) return;
    mutexes[dest].lock();
    auto elem = (static_cast<T*>(variables_storage.at(what).element[source]));
    requests[dest].emplace_back(request_type::var_put, what, source, dest, 0, 0,
                                0, m_shared_ptr(T, *elem));
    mutexes[dest].unlock();
}

/**
 * Inserts a request for replacing an element of a node's private copy of a
 * shared array with the provided value.
 * @tparam T the type of the elements of the array
 * @param what ID of the array to be modified
 * @param source ID of the node that performs the request
 * @param dest ID of the node that holds the private copy of the
 * array that will be modified
 * @param pos position of the element to be modified
 * @param elem the value to be copied inside the destination
 */
template<typename T>
void bsp_communicator::array_put(int what, int source, int dest, int pos,
                                 const T& elem) {
    mutexes[dest].lock();
    requests[dest].emplace_back(request_type::arr_put_el, what, source, dest, 0,
                                pos, 0, m_shared_ptr(T, elem));
    mutexes[dest].unlock();
}

/**
 * Inserts a request for replacing a portion of a node's private copy of a
 * shared array with a portion of the provided array.
 * @tparam T the type of the elements of the arrays
 * @param what ID of the array to be modified
 * @param src ID of the node that performs the request
 * @param src_off starting position of the portion of the replacing array
 * @param dest ID of the node that holds the private copy of the
 * array that will be modified
 * @param dest_off starting position of the portion to be replaced
 * @param len length of the portion of the replacing array
 * @param v the replacing array
 */
template<typename T>
void bsp_communicator::array_put(int what, int src, int src_off, int dest,
                                 int dest_off, int len,
                                 const std::vector<T>& v) {
    mutexes[dest].lock();
    requests[dest].emplace_back(request_type::arr_put, what, src, dest,
                                src_off, dest_off, len,
                                m_shared_ptr(std::vector<T>, v));
    mutexes[dest].unlock();
}

/**
 * Inserts a request for replacing a portion of a node's private copy of a
 * shared array with a portion of the private copy of the requesting node
 * @tparam T the type of the elements of the arrays
 * @param what ID of the array to be modified
 * @param src ID of the node that performs the request
 * @param src_off starting position of the portion of the requesting node's
 * private copy of the array
 * @param dest ID of the node that holds the private copy of the
 * array that will be modified
 * @param dest_off starting position of the portion to be replaced
 * @param len length of the portion of the replacing array
 */
template<typename T>
void bsp_communicator::array_get(int what, int from, int from_off, int to,
                                 int to_off, int len) {
    mutexes[to].lock();
    auto arr = static_cast<std::vector<T>*>(arrays_storage.at(
            what).element[from]);
    requests[to].emplace_back(request_type::arr_get, what, from, to, from_off,
                              to_off, len, m_shared_ptr(std::vector<T>, *arr));
    mutexes[to].unlock();
}

/**
 * Returns the value of a node's private copy of a shared variable,
 * without waiting for a superstep sync.
 * @tparam T the type of the variable
 * @param what ID of the variable to be queried
 * @param source ID of the node that holds the requested private copy
 * @return a copy of the desired value
 */
template<typename T>
T bsp_communicator::variable_direct_get(int what, int source) {
    if (what == 0) return fastflow_input;
    return *(static_cast<T*>(variables_storage.at(what).element.at(source)));
}

/**
 * Returns the value of an element of a node's private copy of a shared array,
 * without waiting for a superstep sync.
 * @tparam T the type of the elements of the array
 * @param what ID of the variable to be queried
 * @param src ID of the node that holds the requested private copy
 * @param pos position of the desired element
 * @return a copy of the desired value
 */
template<typename T>
T bsp_communicator::array_direct_get(int what, int src, int pos) {
    auto arptr = static_cast<std::vector<T>*>(arrays_storage.at(
            what).element.at(src));
    return arptr->at(pos);
}

/**
 * Returns a node's private copy of a shared array, without waiting for a
 * superstep sync.
 * @tparam T the type of the elements of the array
 * @param what ID of the variable to be queried
 * @param src ID of the node that holds the requested private copy
 * @return a copy of the desired array
 */
template<typename T>
std::vector<T> bsp_communicator::array_direct_get(int what, int src) {
    return *(static_cast<std::vector<T>*>(arrays_storage.at(what).element.at(
            src)));
}

/**
 * Requests the handle to a shared variable of type \c T. Will create a new
 * shared variable if the requesting node already has the handle for all
 * already-present shared variables of type T. Sets the private copy of the
 * returned shared variable to an initial value.
 * @tparam T the type of the requested shared variable
 * @param holder ID of the node that requests the handle
 * @param initial_val pointer to the value to be copied inside the requesting
 * node's private copy
 * @return a handle of the shared variable, wrapped in a \c bsp_variable.
 */
template<typename T>
bsp_variable<T> bsp_communicator::get_variable(int holder, T* initial_val) {
    auto tname = typeid(T).name();
    // no. of variables of type T requested by the current worker
    int get_count = 0;
    try {
        get_count = var_count[holder].at(std::string(tname));
    } catch (const std::out_of_range&) {
        var_count[holder].insert({std::string(tname), 0});
    }
    // hash on type, superstep and number of vars
    int hash = get_hash(typeid(T).name(), (generation * 5000000) + get_count);
    int ref;
    // try to find a variable w/ the same hash
    //(i.e. another worker has already requested the creation of the var)
    try {
        // multiple readers-single writer pattern
        std::shared_lock lock(var_mutex);
        ref = variable_dict.at(hash);
    } catch (const std::out_of_range&) {
        std::unique_lock lock(var_mutex);
        // try again, in case another thread created the variable while this one waited
        auto iter = variable_dict.find(hash);
        // if the variable is still not present...
        if (iter == variable_dict.end()) {
            // create the variable...
            inner_var var{nprocs, [](void* el, void* other) {
                auto tptr = static_cast<T*>(el);
                *tptr = *(static_cast<T*>(other));
            }, [](void* el) {
                auto tptr = static_cast<T*>(el);
                delete tptr;
            }};
            ref = variables_storage.size();
            // ..and store it
            variables_storage.push_back(var);
            // together with its hash
            variable_dict.insert({hash, ref});
        } else {
            // the variable is present
            ref = iter->second;
        }
    }
    // initialize with the value requested by the worker
    variables_storage[ref].element.at(holder) = initial_val;
    var_count[holder].at(std::string(tname))++;
    return bsp_variable<T>{ref, holder, this, initial_val};
}

/**
 * Requests the handle to a shared array with elements of type \c T. Will
 * create a new shared array if the requesting node already has the handle for
 * all already-present shared arrayss of type T. Sets the private copy of the
 * returned shared array to an initial value.
 * @tparam T the type of the requested shared variable
 * @param holder ID of the node that requests the handle
 * @param initial_arr pointer to the value to be copied inside the requesting
 * node's private copy
 * @param to_delete flag that indicates whether the array can be deleted
 * safely when perfoming cleanup at the end of the computation
 * @return a handle of the shared array, wrapped in a \c bsp_array.
 */
template<typename T>
bsp_array<T>
bsp_communicator::get_array(int holder, std::vector<T>* initial_arr,
                            bool to_delete) {
    auto tname = typeid(T).name();
    int get_count = 0;
    try {
        get_count = arr_count[holder].at(std::string(tname));
    } catch (const std::out_of_range&) {
        arr_count[holder].insert({std::string(tname), 0});
    }
    int hash = get_hash(typeid(T).name(), (generation * 5000000) + get_count);
    int ref;
    try {
        std::shared_lock lock(arr_mutex);
        ref = array_dict.at(hash);
    } catch (const std::out_of_range&) {
        std::unique_lock lock(arr_mutex);
        auto iter = array_dict.find(hash);
        if (iter == array_dict.end()) {
            inner_array arr{nprocs, [](void* el, void* other, int pos) {
                auto arrptr = static_cast<std::vector<T>*>(el);
                arrptr->at(pos) = *(static_cast<T*>(other));
            }, [](void* el, int srcof, int dstof, int size, void* toput) {
                auto arrptr = static_cast<std::vector<T>*>(el);
                auto otherptr = static_cast<std::vector<T, alloc(T)>*>(toput);
                if (size == 0) {
                    arrptr->resize(otherptr->size());
                    for (size_t idx{0}; idx < otherptr->size(); ++idx) {
                        arrptr->at(idx) = otherptr->at(idx);
                    }
                } else {
                    std::copy_n(otherptr->begin() + srcof, size,
                                arrptr->begin() + dstof);
                }
            }, to_delete ? [](void* el) {
                auto tptr = static_cast<std::vector<T>*>(el);
                delete tptr;
            } : [](void*) {}};
            ref = arrays_storage.size();
            arrays_storage.push_back(arr);
            array_dict.insert({hash, ref});
        } else {
            ref = iter->second;
        }
    }
    arrays_storage[ref].element.at(holder) = initial_arr;
    arr_count[holder].at(std::string(tname))++;
    return bsp_array<T>{ref, holder, this, initial_arr};
}

/**
 * During the superstep synchronization, processes all requests with a
 * certain node as destination. The last node to finish this operation will also
 * advance the superstep count.
 * @param id ID of the node that will process the requests.
 */
void bsp_communicator::process_requests(int id) {
    // Request filtering by worker id
    for (const auto& req: requests[id]) {
        switch (req.t) {
            case request_type::var_put: {
                auto ptr = variables_storage.at(
                        req.reference).element[req.destination];
                variables_storage.at(req.reference).swap(ptr,
                                                         req.element.get());
                break;
            }
            case request_type::arr_put_el: {
                auto ptr = arrays_storage.at(
                        req.reference).element[req.destination];
                arrays_storage.at(req.reference).put(ptr, req.element.get(),
                                                     req.dest_offset);
                break;
            }
            case request_type::arr_put: {
                auto ptr = arrays_storage.at(
                        req.reference).element[req.destination];
                arrays_storage.at(req.reference).replace(ptr, req.src_offset,
                                                         req.dest_offset,
                                                         req.length,
                                                         req.element.get());
                break;
            }
            case request_type::arr_get: {
                auto ptr = arrays_storage.at(
                        req.reference).element[req.destination];
                arrays_storage.at(req.reference).replace(ptr, req.src_offset,
                                                         req.dest_offset,
                                                         req.length,
                                                         req.element.get());
                break;
            }
        }
    }
    // The last worker to finish managing its requests will advance the
    // computation by increasing the superstep count and resetting all the
    // superstep-specific data structures used by the communicator
    if (++process_count == nprocs) {
        generation++;
        delete[] arr_count;
        arr_count = new std::map<std::string, int>[nprocs]();
        delete[] var_count;
        var_count = new std::map<std::string, int>[nprocs]();
        delete[] requests;
        requests = new std::vector<request,alloc(request)>[nprocs]();
        process_count = 0;
    }
}

/**
 * Makes the FastFlow input token available to all BSP nodes.
 * @param in pointer to the input token
 */
void bsp_communicator::set_fastflow_input(const void* in) {
    fastflow_input = in;
}

/**
 * Returns the pointer to the FastFlow input token.
 * @return a \c const pointer to the FastFlow input token.
 */
const void* bsp_communicator::get_fastflow_input() {
    return fastflow_input;
}

/**
 * Deallocates all the data structures used in the communicator and all shared
 * arrays and variables.
 */
void bsp_communicator::end() {

    for (auto& var: variables_storage) {
        auto del = var.free_el;
        for (auto& el: var.element) {
            del(el);
        }
    }

    for (auto& arr: arrays_storage) {
        auto del = arr.free_el;
        for (auto& el: arr.element) {
            del(el);
        }
    }

    delete[] arr_count;
    arr_count = nullptr;
    delete[] var_count;
    var_count = nullptr;
    delete[] requests;
    requests = nullptr;
    delete[] mutexes;
    mutexes = nullptr;
}

#endif //FF_BSP_BSP_COMMUNICATOR_HPP
