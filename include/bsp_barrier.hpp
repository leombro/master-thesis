//
// Created by Orlando Leombruni on 03/11/2019.
//

#ifndef FF_BSP_BSP_BARRIER_HPP
#define FF_BSP_BSP_BARRIER_HPP

#include <mutex>

/**
 * A simple reusable barrier.
 */
class bsp_barrier {
private:

    //! Mutex for access to the barrier
    std::mutex mutex;
    //! Condition variable for the wait/notify mechanism
    std::condition_variable cond_var;
    //! The number of workers that need to access the barrier before
    //! unlocking it.
    int threshold = 0;
    //! Current count of workers at the barrier
    int count = 0;
    //! Number of times the barrier has been used
    int generation = 0;

public:

    /**
     * Deleted default constructor.
     */
    bsp_barrier() = delete;

    /**
     * Creates a barrier for a certain number of threads.
     * @param size the number of threads that will access the barrier
     */
    explicit bsp_barrier(int size) :
            threshold{size},
            count{size},
            generation{0} {
    }

    /**
     * Copy constructor.
     * @param other the other <tt>bsp_barrier</tt> object to copy.
     */
    bsp_barrier(const bsp_barrier& other) :
            threshold{other.threshold},
            count{other.count},
            generation{other.generation} {
    }

    /**
     * Waits on the barrier until all peers do the same.
     */
    void wait() {
        std::unique_lock<std::mutex> lock{mutex};
        auto lastgen = generation;
        if (!(--count)) {
            generation++;
            count = threshold;
            cond_var.notify_all();
        } else {
            cond_var.wait(lock,
                          [this, lastgen]() { return lastgen != generation; });
        }
    }
};

#endif //FF_BSP_BSP_BARRIER_HPP
