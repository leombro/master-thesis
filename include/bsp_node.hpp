//
// Created by Orlando Leombruni on 03/11/2019.
//

#ifndef FF_BSP_BSP_NODE_HPP
#define FF_BSP_BSP_NODE_HPP

#include <ff/ff.hpp>
#include "bsp_communicator.hpp"
#include "bsp_barrier.hpp"


/**
 * Specialization of a ff_node to work as the unit of computation in the BSP
 * model.
 */
class bsp_node: public ff::ff_node {
private:

    //! Number of BSP nodes in this computation
    int nprocs;
    //! ID for this node
    int id = -1;
    //! Pointer to the communicator; implementation of the Communication Channel
    bsp_communicator* comm;
    //! Pointer to the barrier; implementation of the Synchronization Channel
    bsp_barrier* barrier;

    // bsp_program can access fields of this class
    friend class bsp_program;

protected:

    //! Pointer to the FastFlow input token
    const void* fastflow_input;

    /**
     * Forwards an output token to the next stage in the FastFlow graph.
     * @param payload the token to forward
     */
    void emit_output(void* payload) {
        ff_send_out(payload);
    }

    /**
     * Requests a new variable of type T from the communicator.
     * @tparam T the type of the requested variable
     * @param initial_value value to copy inside this node's private copy of
     * the variable
     * @return a handle to this node's private copy of the shared variable
     */
    template <typename T>
    bsp_variable<T> get_variable(const T& initial_value) {
        T* val = new T(initial_value);
        return comm->get_variable<T>(id, val);
    }

    /**
     * Requests a new array with elements of type T from the communicator,
     * initializing it with the copy a given vector.
     * @tparam T the type of elements of the requested array
     * @param initial_value value to copy inside this node's private copy of
     * the array
     * @return a handle to this node's private copy of the shared variable
     */
    template <typename T>
    bsp_array<T> get_array(const std::vector<T>& initial_value) {
        auto val = new std::vector<T>(initial_value);
        return comm->get_array(id, val);
    }

    /**
     * Requests a new array with elements of type T from the communicator,
     * initializing it with the pointer of a vector. Any modifications done
     * to the initializing vector after the call to this method is inherently
     * BSP unsafe.
     * @tparam T the type of elements of the requested array
     * @param handle a pointer to the vector that will become this node's
     * private copy of the shared array
     * @return a handle to this node's private copy of the shared variable
     */
    template <typename T>
    bsp_array<T> get_array(std::vector<T>* handle) {
        return comm->get_array(id, handle, false);
    }

    /**
     * Requests a new array with elements of type T from the communicator,
     * initializing it with an empty vector of given size.
     * @tparam T the type of elements of the requested array
     * @param size the size of the empty vector that will become this node's
     * private copy of the shared array
     * @return a handle to this node's private copy of the shared variable
     */
    template <typename T>
    bsp_array<T> get_empty_array(int size) {
        auto val = new std::vector<T>(size);
        return comm->get_array(id, val);
    }

    /**
     * Returns the ID for this node.
     * @return this node's ID.
     */
    int bsp_pid() {
        return id;
    }

    /**
     * Returns the number of nodes in the current BSP computation.
     * @return the number of nodes in the current BSP computation.
     */
    int bsp_nprocs() {
        return nprocs;
    }

    /**
     * Terminates the current superstep and waits for the other nodes to sync.
     */
    void bsp_sync() {
        barrier->wait();
        comm->process_requests(id);
        barrier->wait();
    }

    /**
     * Function to be overwritten as the main parallel execution.
     */
    virtual void parallel_function() = 0;

public:

    /**
     * The FastFlow node service method.
     *
     * Implementations of this class cannot redefine it.
     * @param in the input token (in this case, a special value \c ENDCOMP)
     * @return the same token as the input
     */
    void* svc (void* in) final {
        fastflow_input = comm->get_fastflow_input();
        parallel_function();
        return in;
    }
};

#endif //FF_BSP_BSP_NODE_HPP
