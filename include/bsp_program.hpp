//
// Created by Orlando Leombruni
//

#ifndef FF_BSP_BSP_PROGRAM_HPP
#define FF_BSP_BSP_PROGRAM_HPP

#include <ff/ff.hpp>
#include <memory>
#include <iostream>
#include "bsp_node.hpp"

/**
 *  Implements the Bulk Synchronous Parallel pattern as a FastFlow node.
 */
class bsp_program : public ff::ff_node {

private:

    /**
     * The emitter of the master-worker scheme; it allows the user to perform a
     * (sequential) function before the execution of the main parallel part.
     */
    struct emitter : ff::ff_node {
        //! Value to end the computation after every node finishes its job
        void* ENDCOMP = (void*) ((unsigned long long) ff::FF_TAG_MIN - 1);

        //! Optional function to be executed before the BSP computation
        std::function<void(void)> preprocessing;
        //! Number of BSP nodes
        int emitter_nprocs;

        /**
         * Constructor for the farm emitter.
         * @param pre optional function to be executed before the BSP
         * computation
         * @param size number of BSP nodes
         */
        emitter(std::function<void(void)> pre, int size) :
                preprocessing{std::move(pre)},
                emitter_nprocs{size} {
        }

        /**
         * Service function of the farm emitter.
         * @param in input FastFlow token
         * @return the End-of-Stream token
         */
        void* svc(void* in) override {
            if (preprocessing != nullptr) preprocessing();
            for (int i = 0; i < emitter_nprocs; i++) {
                ff_send_out(ENDCOMP);
            }
            return EOS;
        }
    };

    /**
     * The collectot of the master-worker scheme; it allows the user to perform
     * a (sequential) function after the execution of the main parallel part.
     */
    struct collector : ff::ff_node {
        //! Value to end the computation after every node finishes its job
        void* ENDCOMP = (void*) ((unsigned long long) ff::FF_TAG_MIN - 1);
        //! Optional function to be executed after the BSP computation
        std::function<void(void)> postprocessing;
        //! Number of nodes in the BSP computation
        int threshold;
        //! Nodes that have finished their local computation
        int count;
        //! Pointer to the the outer class
        bsp_program* master;

        /**
         * Constructor for the farm collector.
         * @param post optional function to be executed after the BSP
         * computation
         * @param size number of BSP nodes
         */
        collector(std::function<void(void)> post, int size) :
                postprocessing{std::move(post)},
                threshold{size},
                count{0} {
        };

        /**
         * Service function of the farm emitter.
         * @param in input FastFlow token
         * @return the input token, or GO_ON
         */
        void* svc(void* in) override {
            if (in == ENDCOMP) {
                if (++count == threshold) {
                    if (postprocessing != nullptr) postprocessing();
                }
                return GO_ON;
            } else {
                master->forward(in);
            }
            return in;
        }
    };

    /**
     * Forwards the received token to the succeeding FastFlow node.
     * @param token the token to be forwarded.
     */
    void forward(void* token) {
        ff_send_out(token);
    }

    //! Number of BSP nodes (workers of the FastFlow farm pattern)
    int nprocs;
    //! Vector of pointers to BSP nodes
    std::vector<std::unique_ptr<bsp_node>> processors;
    //! The entity responsible for communication between nodes
    bsp_communicator comm;
    //! A barrier for synchronization between supersteps
    bsp_barrier barr;
    //! The farm emitter
    emitter E;
    //! The farm collector
    collector C;

public:

    /**
     * Constructor for the bsp_program object. Pushes necessary information
     * into the relevant entities (emitter, workers, collector)
     * @param _processors vector of BSP nodes for the computation
     * @param _pre optional function to be executed before the BSP computation
     * @param _post optional function to be executed after the BSP computation
     */
    explicit bsp_program(std::vector<std::unique_ptr<bsp_node>>&& _processors,
                         std::function<void(void)> _pre = nullptr,
                         std::function<void(void)> _post = nullptr) :
            nprocs{static_cast<int>(_processors.size())},
            comm{nprocs},
            barr{nprocs},
            E{std::move(_pre), nprocs},
            processors{std::move(_processors)},
            C{std::move(_post), nprocs} {
        for (size_t i{0}; i < nprocs; ++i) {
            processors[i]->nprocs = nprocs;
            processors[i]->id = i;
            processors[i]->barrier = &barr;
            processors[i]->comm = &comm;
            C.master = this;
        }
    };

    /**
     * Creates the FastFlow inner graph and executes the BSP computation.
     * @param in optional, the input token received from the preceding FastFlow
     * node
     */
    void start(void* in = nullptr) {
        std::vector<std::unique_ptr<ff::ff_node>> workers;
        for (size_t i{0}; i < nprocs; ++i) {
            auto d = static_cast<ff_node*>(processors[i].release());
            workers.emplace_back(std::unique_ptr<ff_node>(d));
        }

        comm.set_fastflow_input(in);

        ff::ff_Farm<> farm(std::move(workers), E, C);

        if (farm.run_and_wait_end() < 0)
            std::cout << "error in running farm" << std::endl;

        comm.end();
    }

    /**
     * Service function for the BSP program node. Starts the BSP computation.
     * @param in input token received from the preceding FastFlow node
     * @return GO_ON
     */
    void* svc(void* in) override {
        start(in);
        return GO_ON;
    }
};

#endif //FF_BSP_BSP_PROGRAM_HPP
